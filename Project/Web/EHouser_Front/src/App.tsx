import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {BrowserRouter as Router, Link, Route, Redirect} from 'react-router-dom';
import { connect } from 'react-redux';
import {bindActionCreators} from "redux";

import AppViewer from './components/AppViewer';

import './css/App.less';

export class App extends React.Component<any,any> {

  constructor(props: any) {
    super(props);
    this.state = {
     
    }

  };



  render() {
    return (
      <Router>
        <div className="app full-height">
            <AppViewer {...this.props} />
        </div>
      </Router>
    );
  }
}
