export function scrollIntoPage(dest: string) {
    return {
        type: "scrollIntoPage",
        payload: {
            dest: dest,
            type: "scrollIntoPage",
        }
    }
}

export function hideMenu() {
    return {
        type: "showMenu",
        payload: {
            show: false,
            type: "showMenu",
        }
    };
}

export function showMenu() {
    return {
        type: "showMenu",
        payload: {
            show: true,
            type: "showMenu",
        }
    };
}

export function showAlert(show:boolean, message:string, alertMode:string) {
    return {
        type: "showAlert",
        payload: {
            show: show,
            message: message,
            alertType:alertMode,
            type: "showAlert"
        }
    };
}

export function hideAlert() {
    return {
        type: "showAlert",
        payload: {
            show: false,
            message: "",
            type: ""
        }
    };
}
