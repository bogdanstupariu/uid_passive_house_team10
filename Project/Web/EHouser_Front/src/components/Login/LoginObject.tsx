export class LoginObject {
    public email : {value: string, isValid: boolean};
    public password : {value: string, isValid: boolean};
    public verified : boolean;
}


export class RegisterObject {
    public password : {value: string, isValid: boolean};
    public passwordRepeat : {value: string, isValid: boolean};
    public email : {value: string, isValid: boolean};
    public firstname : {value: string, isValid: boolean};
    public lastname : {value: string, isValid: boolean};
    public phoneNumber : {value: string, isValid: boolean};
    public verified : boolean;
    public passwordMatch : boolean;
}