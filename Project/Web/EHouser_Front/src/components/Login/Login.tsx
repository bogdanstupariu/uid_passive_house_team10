import * as React from "react"
import * as ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import "../../css/Login.less"
import "../../css/App.less"
import { hideMenu, showAlert, hideAlert } from '../../store/actions';
import { InputDesigned } from '../Utility/InputDesigned';
import {Validator} from '../../services/Validation';
import {LoginObject, RegisterObject} from './LoginObject'
import { LoginServices } from "../../services/LoginServices";


interface IStateInterface {
    width: number,
    height: number,
    showRegister: boolean,
    email : {
        value: string,
        isValid: boolean
    },
    password : {
        value: string,
        isValid: boolean
    },
    passwordre : {
        value: string,
        isValid: boolean
    },
    firstname : {
        value: string,
        isValid: boolean
    },
    lastname : {
        value: string,
        isValid: boolean
    },
    phone : {
        value: string,
        isValid: boolean
    },
    isLogged:boolean,
    regAsSpec:any;

}

class Login extends React.Component<any,
IStateInterface> {

    constructor(props: any) {
        super(props);
        this.state = {
            width: 0,
            height: 0,
            showRegister: false,
            email : {
                value: "",
                isValid: true
            },
            password : {
                value: "",
                isValid: true
            },
            passwordre : {
                value: "",
                isValid: true
            },
            firstname : {
                value: "",
                isValid: true
            },
            lastname : {
                value: "",
                isValid: true
            },
            phone : {
                value: "",
                isValid: true
            },
            isLogged:false,
            regAsSpec:false
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        //this.props.dispatch(hideMenu());
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
        this.setState({showRegister: this.props.showRegister});
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    componentDidUpdate() {

    }

    loginSucceeded() {
        this
            .props
            .isLoged(this.state.isLogged);
        this.props.history.push("/");
    }

    changeView(type:any,data:any){
        if(type == "reg")
            this.setState({showRegister: true});
        else
            this.setState({showRegister: false});
    }

    bindData= (type : any, text : any) => {
        if(type == "checkbox"){
            this.setState({regAsSpec:text.target.checked});
        } else {
            var content = {
                value: text,
                isValid: true
            };
            this.setState({[type]: content});
        }
    }

    tryToLog(){
        var loginObject = new LoginObject();
        loginObject.password = this.state.password;
        loginObject.email = this.state.email;
        loginObject.verified = true;

        loginObject = Validator.validateLogin(loginObject);
        this.setState({password: loginObject.password});
        this.setState({email: loginObject.email});
        if (loginObject.verified === false) {
            if (loginObject.email.isValid === false) {
                this.props.dispatch(showAlert(true,"Wrong email format","red-alert"));
            } else if (loginObject.password.isValid === false) {
                this.props.dispatch(showAlert(true,"Password too short","red-alert"));
            }
        } else {
            this.props.dispatch(hideAlert());
            var loginResult = LoginServices.tryToLog(this, loginObject);
        }
    }

    tryToReg(){
        var registerObject = new RegisterObject();
        registerObject.email = this.state.email;
        registerObject.password = this.state.password;
        registerObject.passwordRepeat = this.state.passwordre;
        registerObject.firstname = this.state.firstname;
        registerObject.lastname = this.state.lastname;
        registerObject.phoneNumber = this.state.phone;
        registerObject.verified = true;
        registerObject.passwordMatch = true;

        registerObject = Validator.validateRegister(registerObject);

        this.setState({email: registerObject.email});
        this.setState({password: registerObject.password});
        this.setState({passwordre: registerObject.passwordRepeat});
        this.setState({firstname: registerObject.firstname});
        this.setState({lastname: registerObject.lastname});
        this.setState({phone: registerObject.phoneNumber});

        if (registerObject.verified === false) {
            if (registerObject.email.isValid === false) {
                this.props.dispatch(showAlert(true,"Invalid email. Please use your real E-mail address", "red-alert"));
            } else if (registerObject.password.isValid === false) {
                this.props.dispatch(showAlert(true,"Password too short", "red-alert"));
            }else if (registerObject.passwordMatch === false) {
                this.props.dispatch(showAlert(true,"Passwords should match", "red-alert"));
            }else if (registerObject.firstname.isValid === false) {
                this.props.dispatch(showAlert(true,"Invalid name. Capitalize Each Word", "red-alert"));
            }else if (registerObject.lastname.isValid === false) {
                this.props.dispatch(showAlert(true,"Invalid name. Capitalize Each Word", "red-alert"));
            }else if (registerObject.phoneNumber.isValid === false) {
                this.props.dispatch(showAlert(true,"Invalid phone number. Please enter your real phone number", "red-alert"));
            }
        } else {
            this.props.dispatch(hideAlert());
            LoginServices.insertUser(this, registerObject);
        }
    
    }

    registerSuccess(){
       this.props.dispatch(showAlert(true,"Your account has been created. You can login now","green-alert"));
       this.setState({showRegister: false});
    }

    render() {

        return (
            <div className="login " style={{ "height": this.state.height }}>
                <div className="landing ">
                    <div className="layer">

                    </div>
                </div>
                <div className="overpose">
                        <div className="half-width-container">
                            <div className="half-width text-layer">
                                <div className={"text-layer-container " + (this.state.showRegister ? "hidden" : "")}>
                                    <h1><a>Register now!</a></h1>
                                    <p>Get registered now and be part of <b>PASSIVE EHOUSE</b> community.</p>
                                    <p>get help, support and advices for bulding your own passive house during</p>
                                    <p> <b>design, implementation and monitorization phases</b></p>
                                    <button onClick={this.changeView.bind(this,"reg")}>Register</button>
                                </div>
                                <div className={"text-layer-container " + (this.state.showRegister ? "" : "hidden")}>
                                    <h1><a>Log In now!</a></h1>
                                    <p>If you already have an account on <b>PASSIVE EHOUSE</b> community.</p>
                                    <p>sign in now to access your account</p>
                                    <button onClick={this.changeView.bind(this,"log")}>Login</button>
                                </div>
                            </div>
                            <div className="half-width">
                                <div className={"loginView " + (this.state.showRegister ? "hidden" : "")}>
                                    <div className="loginView-header">
                                        <i className="fa fa-pencil fa-3x" aria-hidden="true"></i>
                                        <h1>Log in now</h1>
                                        <p>Log in into your account and have access to Passive EHouse's dashboard</p>
                                    </div>
                                    <div className="loginView-container">
                                        <InputDesigned  placeholder="email"
                                            type="text"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.email.isValid}
                                            data={this.bindData.bind(this,"email")}/>
                                        <InputDesigned  placeholder="password"
                                            type="password"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.password.isValid}
                                            data={this.bindData.bind(this,"password")}/>
                                        <button onClick={this.tryToLog.bind(this)}>Log in</button>
                                    </div>
                                </div>
                                <div className={"registerView " + (this.state.showRegister ? "" : "hidden")}>
                                <div className="loginView-header">
                                        <i className="fa fa-pencil fa-3x" aria-hidden="true"></i>
                                        <h1>Sing up now</h1>
                                        <p>Create an account and become a member of Passive EHouse's community</p>
                                    </div>
                                    <div className="loginView-container">
                                        <InputDesigned  placeholder="email"
                                            type="text"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.email.isValid}
                                            data={this.bindData.bind(this,"email")}/>
                                        <InputDesigned  placeholder="password"
                                            type="password"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.password.isValid}
                                            data={this.bindData.bind(this,"password")}/>
                                        <InputDesigned  placeholder="repeat password"
                                            type="password"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.passwordre.isValid}
                                            data={this.bindData.bind(this,"passwordre")}/>
                                        <InputDesigned  placeholder="firstname"
                                            type="text"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.firstname.isValid}
                                            data={this.bindData.bind(this,"firstname")}/>
                                        <InputDesigned  placeholder="lastname"
                                            type="text"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.lastname.isValid}
                                            data={this.bindData.bind(this,"lastname")}/>
                                        <InputDesigned  placeholder="phone number"
                                            type="text"
                                            glyph="glyphicon-lock"
                                            isValid={this.state.phone.isValid}
                                            data={this.bindData.bind(this,"phone")}/>
                                        <span style={{"display":"flex"}}>
                                            <input onChange={this.bindData.bind(this,"checkbox")}
                                            defaultChecked={false}
                                                style={{    "width": "14px",
                                                    "margin": "-3px 10px 0 0"}}
                                                type="checkbox"/>
                                            Register as specialist
                                        </span>
                                        <button onClick={this.tryToReg.bind(this)}>Sing up</button>
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        );

    }
}


function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(Login as any);