import * as React from "react"
import * as ReactDOM from 'react-dom';

import {BrowserRouter as Router, Link, Route, Redirect} from 'react-router-dom';

import {connect} from 'react-redux';
import {withRouter} from "react-router";

import Homepage from "./Homepage";
import Header from './../components/Header';
import {Footer} from './../components/Footer';
import { scrollIntoPage, hideAlert } from '../store/actions';
import Login from './Login/Login';
import { Danger_Alert } from "./Utility/Danger_Alert";
import Dashboard from "./Dashboard";


interface IStateInterface {
    headerVisible:boolean;
    showRegister:boolean;
    isLogged:boolean;
}

class AppViewer extends React.Component < any,
IStateInterface > {
    constructor(props : any) {
        super(props);

        var log = false;
        if(localStorage.getItem("access_token")!=null){
           log= true;
        }

        this.state = {
            headerVisible: true,
            showRegister: false,
            isLogged:log
        };

    }

    componentDidMount() {
    
    }

    dataBind = (param : any, text : any) => {
        if(param == "showreg"){
            this.setState({showRegister:true});
            this.props.history.push('/login');
        }
        if(param == "log"){
            if(text == true)
                this.setState({isLogged: text});
        }
        if(param == "log-out"){
            this.setState({isLogged: !text});
            localStorage.removeItem("userName");
            localStorage.removeItem("access_token");
            this.props.history.push("/");
        }
    }

    notDanger() {
        this.props.dispatch(hideAlert());
    }

    render() {
        return (
            <div className="full-height ">
                <div className="row alert-boxed">
                    <Danger_Alert
                        message={(this.props.alert != undefined ?this.props.alert.message : "")}
                        visible={(this.props.alert != undefined ?this.props.alert.show:"")}
                        dismised={this
                        .notDanger
                        .bind(this)}
                        type={(this.props.alert != undefined ? this.props.alert.alertType : "")}/>
                </div>
                <Header {...this.props} isLogged = {this.state.isLogged} logOut = {this.dataBind.bind(this,"log-out")}/>
                <Route path='/' exact render={(props) => (<Homepage {...props} goToRegister={this.dataBind.bind(this,"showreg")}/>)}/>
                <Route path='/login' exact render={(props) => (<Login {...props} showRegister={this.state.showRegister} isLoged={this.dataBind.bind(this, "log")}/>)}/>
                <Route path='/dashboard' exact render={(props) => (
                    (this.state.isLogged ? <Dashboard {...props} logOut = {this.dataBind.bind(this,"log-out")}/> : <Redirect to={"/"}/>)
                )}/>
                <Footer/>
            </div>

        );

    }

}

function mapStateToProps(state : any, history : any) {
    if(state.type == "showMenu"){
        return {
            myStore: {
                type: state.type,
                show: state.show
            }

        }
    } if(state.type == "showAlert"){
        return {
            alert: {
                message: state.message,
                show: state.show,
                type: state.type,
                alertType: state.alertType
            }
        }
    }
    else {
        return {
            myStore: {
                type: state.type
            }

        }
    }
}


var connectedAppViewer = connect(mapStateToProps)(AppViewer as any);

export default withRouter(connectedAppViewer);