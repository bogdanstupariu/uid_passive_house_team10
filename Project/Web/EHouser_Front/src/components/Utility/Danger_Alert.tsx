import * as React from "react"
import * as ReactDOM from 'react-dom';

import "./alert.less";

interface IPropsInterface {
    message : string;
    visible : boolean;
    dismised: any;
    type : string;
}

interface IStateInterface {
    isVisible : boolean;
}

export class Danger_Alert extends React.Component < IPropsInterface,
any > {

    constructor(props:any){
        super(props);
        this.state = {
            isVisible: false
        }
    }

    hide(){
        this.props.dismised();
    }

    onComponentDidUpdate(){
        console.log(this.props);
    }

    render() {
        var disabled;
        {this.props.visible? disabled=null : disabled="danger_disabled"}
        return (
          
                <div className={`alert alert-danger ${disabled} ${this.props.type}`}>
                    <a onClick={this.hide.bind(this)} className="close_icon pull-right"  aria-label="close">&times;</a>
                    <p><b>{this.props.message}</b></p>
                </div>
        );

    }

}
