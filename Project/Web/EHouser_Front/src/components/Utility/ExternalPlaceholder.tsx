import * as React from "react"

import "./input.less";

interface IPropsInterface {
    placeholder : string;

}

interface IStateInterface {
};

export class ExternalPlaceholder extends React.Component < IPropsInterface,
IStateInterface > {

    constructor(props: any) {
        super(props);

    }


    render() {
        return (
            <div className="fixed_place">
                <p>{this.props.placeholder}</p>
            </div>
        );
    }

}
