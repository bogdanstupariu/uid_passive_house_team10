import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import {withRouter} from "react-router";
import { scrollIntoPage } from '../store/actions';

import "../css/Header.less"
import "../css/App.less"

interface ComponentProps {
    isVisible:boolean;
}


interface IStateInterface{
    fixed: boolean;
    headerVisible: boolean;
    isLogged: boolean;
}


class Header extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state= {
            fixed: false,
            headerVisible: true,
            isLogged:false
        }

        window.addEventListener('scroll', (event: any) => {
            var offset = window.pageYOffset;
            if(offset > 400){
                this.setState({fixed:true});
            } else {
                this.setState({fixed:false});
            }

        });

    }

    logOut(){
        this.props.logOut(true);
    }

    componentDidUpdate(){
        if(this.state.isLogged != this.props.isLogged){
             this.setState({isLogged:this.props.isLogged})
        }
        if(this.props.menu.show != undefined){
            if(this.state.headerVisible != this.props.menu.show){
                this.setState({headerVisible: this.props.menu.show})
            }
        }
    }


    handleScrollToElement = (data:any, e:any) => {
        if(this.props.location.pathname != "/"){
            this.props.history.push('/');
        }
        this.props.dispatch(scrollIntoPage(data));
    }

    render() {
        var username;
        if(localStorage.getItem("access_token")!=null)
            username = localStorage.getItem("userName");
        return (
            <div id="test" className={"header " + (this.state.fixed ? 'fixed ' : ' ')  + 
                    (this.state.headerVisible ? '  ' : '  hidden') }>
                <div >
                    <img src="../../public/eco-logo.png"/>
                </div> 
                <ul>
                    <li onClick={this.handleScrollToElement.bind(this,"6")}>
                        Home
                    </li>
                    <li onClick={this.handleScrollToElement.bind(this,"1")}>
                        About
                    </li>

                    <li onClick={this.handleScrollToElement.bind(this,"2")}>
                        Examples
                    </li>

                    <li onClick={this.handleScrollToElement.bind(this,"3")}>
                        Solar energy
                    </li>

                    <li onClick={this.handleScrollToElement.bind(this,"4")}>
                        Pros&Cons
                    </li>

                    <li className={(this.state.isLogged?" hidden" :"")} onClick={this.handleScrollToElement.bind(this,"5")}>
                        <i className="fa fa-user-circle" aria-hidden="true"></i>
                        Log in
                    </li>
                    <li className={"dashboardmenu "+(this.state.isLogged?" " :"hidden")} >
                        <div onClick={this.handleScrollToElement.bind(this,"7")}>
                            <i className="fa fa-user-circle" aria-hidden="true"></i>
                                Profile
                        </div>
                        <div className="logMenu">
                            <ul>
                                <li onClick={this.handleScrollToElement.bind(this,"8")}>Dashboard</li>
                                <li onClick={this.logOut.bind(this)}>Log out</li>
                            </ul>
                        </div>
                    </li>
                </ul>

            </div>
        );

    }

}

function mapStateToProps(state : any, props:any) {
    return {
        menu: {
            show: state.show,
            type: state.type,
        }
    };
}

export default connect(mapStateToProps)(Header as any);

