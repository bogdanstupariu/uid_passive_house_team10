import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import ReactLoading from 'react-loading';

import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../store/actions';

import "../css/App.less"
import "../css/Dashboard.less"
import MyProfile from "./Dashboard/MyProfile";
import MyProject from "./Dashboard/MyProject";
import CreateProject from "./Dashboard/CreateProject";
import CostsMaterials from "./Dashboard/CostsMaterials";
import GeoLocation from "./Dashboard/GeoLocation";
import OffersAdvices from "./Dashboard/OffersAdvices";
import AdminPage from "./Dashboard/AdminPage";
import Diplomas from "./Dashboard/Diplomas";
import DashHome from "./Dashboard/DashHome";

interface ComponentProps {
    isVisible: boolean;
    logOut: any;
}


interface IStateInterface {
    isLogged: boolean;
    role: {
        user: boolean,
        admin: boolean,
        specialist: boolean
    }
    width: number,
    height: number,
    pageToShow: string,
    showSidebar: boolean;
    clickSidebar: boolean;
    userProfile: any;
    userUri: string;
    isLoading:boolean;
    specialistDiplomas: any;
    houseProject:any;
    allHouseProjects:any;
}


class Dashboard extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);

        this.state = {
            isLogged: false,
            role: {
                user: false,
                admin: false,
                specialist: false
            },
            width: 0,
            height: 0,
            pageToShow: "dashhome",
            showSidebar: true,
            clickSidebar: false,
            userProfile: null,
            userUri: "",
            isLoading:false,
            specialistDiplomas:[],
            houseProject:[],
            allHouseProjects:[]
        }
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.props.dispatch(hideMenu());
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    componentDidMount() {
        this.updateWindowDimensions();
        this.setState({pageToShow:"dashhome"});
        window.addEventListener('resize', this.updateWindowDimensions);

        if (this.state.width < 900)
            this.setState({ showSidebar: false });
        else
            this.setState({ showSidebar: true });

        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var request = new Request("http://localhost:2766/api/Account/GetLoggedInUserRole", {
            method: "GET",
            headers: myheader
        });

        component.setState({isLoading:true});
        let response = fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                var type = {
                    user: false,
                    admin: false,
                    specialist: false
                }
                if (data == "CommonUser") {
                    type.user = true;
                    type.admin = false;
                    type.specialist = false;
                    component.setState({ role: type })
                } else {
                    if (data == "Specialist") {
                        type.user = false;
                        type.admin = false;
                        type.specialist = true;
                        component.setState({ role: type })
                    }
                    else
                        if (data == "Administrator") {
                            type.user = false;
                            type.admin = true;
                            type.specialist = false;
                            component.setState({ role: type })
                        }
                }
            })
            .then(function() { 
                component.getLoggedInUserDetails();
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })



    }

    getLoggedInUserDetails() {
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var request = new Request("http://localhost:2766/api/Account/GetLoggedInUser", {
            method: "GET",
            headers: myheader
        });
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                component.setState({ userProfile: data })
            })
            .then(function() { 
                component.setUri() 
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    getSpecialistDiplomas(){
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var request = new Request("http://localhost:2766/api/Diploma/GetDiplomasOfSpecificSpecialist?Id=" + this.state.userUri, {
            method: "GET",
            headers: myheader
        });
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                component.setState({ specialistDiplomas: data })
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    getAllDiplomas() {
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var request = new Request("http://localhost:2766/api/Diploma/GetAllDiplomas", {
            method: "GET",
            headers: myheader
        });
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                var sortedData = data.sort(function(a:any, b:any) {
                    return a.Verified - b.Verified
                });
                component.setState({specialistDiplomas:sortedData})
                component.setNotLoading()
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    getUserProjectHouse(){
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var request = new Request("http://localhost:2766/api/PassiveHouse/GetAllHouses", {
            method: "GET",
            headers: myheader
        });
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                if(data!=null){
                    component.setState({allHouseProjects:data})
                    data.forEach((element:any) => {
                        if(element.UserID == component.state.userUri){
                            component.setState({houseProject:element})
                        }
                    });
                }
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    setUri(){
        this.setState({ userUri: this.state.userProfile.Id },()=>{
            this.getUserProjectHouse();
            if(this.state.role.specialist)
                this.getSpecialistDiplomas();
            if(this.state.role.admin)
                this.getAllDiplomas();
        });
    }

    setNotLoading(){
        this.setState({isLoading:false});        
    }

    logOut() {
        this.props.logOut(true);
    }

    componentDidUpdate() {
        if (this.state.width < 900) {
            if (this.state.showSidebar == true)
                this.setState({ showSidebar: false });
        }
        else {
            if (this.state.showSidebar == false)
                this.setState({ showSidebar: true });
        }
    }

    dataBind = (param: any, text: any) => {
        if (param == "goHome") {
            this.props.dispatch(showMenu());
            this.props.history.push('/');
        }
        if (param == "logOut") {
            localStorage.removeItem("userName");
            localStorage.removeItem("access_token");
            this.props.logOut(true);
            this.props.history.push("/");
        }
        if (param == "profile")
            this.setState({ pageToShow: "profile" });
        if (param == "project")
            this.setState({ pageToShow: "project" });
        if (param == "create")
            this.setState({ pageToShow: "create" });
        if (param == "costs")
            this.setState({ pageToShow: "costs" });
        if (param == "geo")
            this.setState({ pageToShow: "geo" });
        if (param == "advice")
            this.setState({ pageToShow: "advice" });
        if (param == "admin")
            this.setState({ pageToShow: "admin" });
        if(param == "diplomas")
            this.setState({pageToShow: "diplomas"});
        if(param == "dashhome")
            this.setState({pageToShow: "dashhome"});
        if(param == "loading"){
            if(this.state.isLoading != text)
                this.setState({isLoading:text});
        }
        
    }

    changeSidebarShow() {
        this.setState({ clickSidebar: !this.state.clickSidebar })
    }

    render() {
        var shouldShow = this.state.showSidebar || this.state.clickSidebar;
        return (
            <div className="dashboard" style={{ "height": this.state.height }}>
                <div className="mobile-sidebar" onClick={this.changeSidebarShow.bind(this)}>
                    <i className={"fa " + (!shouldShow ? "fa-bars" : "fa-window-minimize")} aria-hidden="true"></i>
                </div>
                <div className={"sidebar " + (shouldShow ? "maximize" : "minimize")}>
                    <div className="sidebar-image">
                        <img src="../../public/eco_house_logo.png" />
                    </div>
                    <div className="label">
                        Dashboard
                    </div>

                    <div className="action-container">
                        <div className={(this.state.role.admin ? "hidden" : "")}>
                            <button onClick={this.dataBind.bind(this, "profile")}>My profile</button>
                        </div>
                        <div className="menu-projects">
                            <button className="show-me-the-menu">EHouse project</button>
                            <div className="menu">
                                <div>
                                    <button onClick={this.dataBind.bind(this, "create")}>Create project</button>
                                </div>
                                <div>
                                    <button onClick={this.dataBind.bind(this, "project")}>My project</button>
                                </div>
                                <div>
                                    <button onClick={this.dataBind.bind(this, "costs")}>Costs&Materials</button>
                                </div>
                                <div>
                                    <button onClick={this.dataBind.bind(this, "geo")}>Geo Location</button>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button onClick={this.dataBind.bind(this, "advice")}>Offers&Advices</button>
                        </div>
                        <div className={(this.state.role.user ? "" : "hidden")}>
                            <button onClick={this.dataBind.bind(this, "dashhome")}>Plan</button>
                        </div>
                        <div className={(this.state.role.admin ? "" : "hidden")}>
                            <button onClick={this.dataBind.bind(this, "admin")}>Admin</button>
                        </div>
                        <div className={(this.state.role.user ? "hidden" : "")}>
                            <button onClick={this.dataBind.bind(this, "diplomas")}>Diplomas</button>
                        </div>
                    </div>

                    <div className="bottom-container">
                        <div>
                            <button onClick={this.dataBind.bind(this, "goHome")}>Homepage</button>
                        </div>
                        <div>
                            <button onClick={this.dataBind.bind(this, "logOut")}>Log out</button>
                        </div>
                    </div>
                </div>
                <div className="content" style={{ "width": (this.state.width - 200) }}>
                
                    <div className={"spinner " + (this.state.isLoading?"":"hidden")}>
                        <ReactLoading type="spinningBubbles" color="#444" height='667' width='375' />
                    </div>
                    <DashHome {...this.props} enabled={(this.state.pageToShow == "dashhome")}
                        width={this.state.width}/>
                    
                    <MyProfile {...this.props} enabled={(this.state.pageToShow == "profile")}
                        userProfile={this.state.userProfile}
                        isSpecialist={this.state.role.specialist}
                        isLoading={this.dataBind.bind(this,"loading")}
                    />
                    <MyProject {...this.props} enabled={(this.state.pageToShow == "project")} 
                        isLoading={this.dataBind.bind(this,"loading")}
                        houseProject ={this.state.houseProject}/>
                    <CreateProject {...this.props} enabled={(this.state.pageToShow == "create")} 
                        isLoading={this.dataBind.bind(this,"loading")}/>
                    <CostsMaterials {...this.props} enabled={(this.state.pageToShow == "costs")} 
                        isLoading={this.dataBind.bind(this,"loading")}
                        houseProject={this.state.houseProject}/>
                    <GeoLocation {...this.props} enabled={(this.state.pageToShow == "geo")} 
                        isLoading={this.dataBind.bind(this,"loading")}
                        houseProject={this.state.houseProject}/>
                    <OffersAdvices {...this.props} enabled={(this.state.pageToShow == "advice")}
                        role={this.state.role}
                        userUri={this.state.userUri}
                        isLoading={this.dataBind.bind(this,"loading")}
                        specialistDiplomas={this.state.specialistDiplomas}
                    />
                    <AdminPage {...this.props} enabled={(this.state.pageToShow == "admin")} 
                        isLoading={this.dataBind.bind(this,"loading")}
                        allHouseProjects={this.state.allHouseProjects}/>
                    {this.state.role.user ? "" :
                        <Diplomas {...this.props} enabled={(this.state.pageToShow == "diplomas")} 
                            isLoading={this.dataBind.bind(this,"loading")}
                            isAdmin={this.state.role.admin}
                            specialistDiplomas={this.state.specialistDiplomas}
                            />
                    }
                </div>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(Dashboard as any);