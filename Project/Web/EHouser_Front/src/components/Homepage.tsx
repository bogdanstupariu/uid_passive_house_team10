import * as React from "react";
import * as ReactDOM from 'react-dom';
import * as base64 from 'base-64';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import "../css/Homepage.less"
import "../css/Loader.less"
import "../css/App.less"
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, hideMenu, showMenu } from '../store/actions';


interface IStateInterface {
    height: any;
    width: any;
    menuSelected: {
        value: string,
        date: string
    }
};



class Homepage extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            width: 0,
            height: 0,
            menuSelected: { value: "0", date: "0" }
        };
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.props.dispatch(showMenu());
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    componentDidUpdate() {
        this.checkMenuClick(this.props.store.dest);
    }

    checkMenuClick(type: string) {
        switch (type) {

            case ("1"):
                this.scrollToElem("about-passive");
                break;
            case ("2"):
                this.scrollToElem("passive-examples");
                break;
            case ("3"):
                this.scrollToElem("solar-energy");
                break;
            case ("4"):
                this.scrollToElem("why-passive");
                break;
            case ("6"):
                this.scrollToElem("home");
                break;
            case ("5"):
                this.props.history.push('/login');
                //this.props.dispatch(hideMenu());
                break;
            case ("8"):
                this.props.history.push('/dashboard');
                this.props.dispatch(hideMenu());
                break;
        }
    }

    scrollToElem(dest: string) {
        var elem = document.getElementById(dest);
        if (elem != null)
            elem.scrollIntoView({ behavior: 'smooth' });
        this.props.dispatch(scrollIntoPage("0"));
    }

    goToRegister() {
        this.props.goToRegister(true);
    }



    render() {
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 3,
            slidesToScroll: 1,
            centerMode: true,
        };
        var settings2 = {
            dots: true,
            infinite: false,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1
        };
        return (

            <div className="homepage full-height" >

                <div id="home" className="landing" style={{ "height": this.state.height }}>
                    <img src="../../public/eco-logo.png" />
                </div>
                <div id="about-passive" className="about-passive container">
                    <div className="half-width-container">
                        <div className="half-width definition">
                            <h2>What is a passive house?</h2>
                            <p>
                                A building standard that is truly energy efficient, comfortable, affordable and ecological at the same time.
                                Passive House is not a brand name, but a construction concept that can be applied by anyone and that has stood the test of practice.
                            </p>
                            <p>
                                Yet, a Passive House is more than just a low-energy building:
                            </p>
                        </div>
                        <div className="half-width align-center">
                            <img src="../../public/heating_energy_savings_diagram_e.jpg" />
                        </div>
                    </div>
                    <div className="full-width list-descript">
                        <ul>
                            <li><i className="fa fa-home fa-2x fa-pull-left" aria-hidden="true"></i>
                                Passive Houses allow for space heating and cooling related energy savings of up to 90% compared with typical building stock and over 75% compared to average new builds. Passive Houses use less than 1.5 l of oil or 1.5 m3 of gas to heat one square meter of living space for a year – substantially less than common “low-energy” buildings. Vast energy savings have been demonstrated in warm climates where typical buildings also require active cooling.
                                    </li>
                            <li><i className="fa fa-home fa-2x fa-pull-left" aria-hidden="true"></i>
                                Passive Houses make efficient use of the sun, internal heat sources and heat recovery, rendering conventional heating systems unnecessary throughout even the coldest of winters. During warmer months, Passive Houses make use of passive cooling techniques such as strategic shading to keep comfortably cool.
                                    </li>
                            <li><i className="fa fa-home fa-2x fa-pull-left" aria-hidden="true"></i>
                                Passive Houses are praised for the high level of comfort they offer. Internal surface temperatures vary little from indoor air temperatures, even in the face of extreme outdoor temperatures. Special windows and a building envelope consisting of a highly insulated roof and floor slab as well as highly insulated exterior walls keep the desired warmth in the house – or undesirable heat out.
                                    </li>
                            <li><i className="fa fa-home fa-2x fa-pull-left" aria-hidden="true"></i>
                                A ventilation system imperceptibly supplies constant fresh air, making for superior air quality without unpleasant draughts. A highly efficient heat recovery unit allows for the heat contained in the exhaust air to be re-used.
                                    </li>
                        </ul>
                    </div>
                </div>

                <div id="passive-examples" className="passive-examples">
                    <h1>Passive house examples</h1>
                    <Slider {...settings}>
                        <div>
                            <div className="contain-image">
                                <img src="../../public/passive-houses/passive1.jpg" className="carousel-image" />
                            </div>
                        </div>
                        <div>
                            <div className="contain-image">
                                <img src="../../public/passive-houses/passive2.jpg" className="carousel-image" />
                            </div>
                        </div>
                        <div>
                            <div className="contain-image">
                                <img src="../../public/passive-houses/passive3.jpg" className="carousel-image" />
                            </div>
                        </div>
                        <div>
                            <div className="contain-image">
                                <img src="../../public/passive-houses/passive4.jpg" className="carousel-image" />
                            </div>
                        </div>
                        <div>
                            <div className="contain-image">
                                <img src="../../public/passive-houses/passive5.jpg" className="carousel-image" />
                            </div>
                        </div>
                        <div>
                            <div className="contain-image">
                                <img src="../../public/passive-houses/passive6.jpg" className="carousel-image" />
                            </div>
                        </div>
                        <div>
                            <div className="contain-image">
                                <img src="../../public/passive-houses/passive7.jpg" className="carousel-image" />
                            </div>
                        </div>
                    </Slider>

                </div>

                <div className="gif-container">
                    <img src="../../public/solar-power-for-home.gif" className="Image-gif" />
                </div>

                <div className="container principles">
                    <h1>Passive House principles</h1>
                    <p>Passive building comprises a set of design principles used to attain a quantifiable and rigorous level of
                        energy efficiency within a specific quantifiable comfort level. “Maximize your gains, minimize your losses”
                        summarizes the approach. To that end, a passive building is designed and built in accordance with these five
                        building-science principles: </p>
                    <ul>
                        <li>Employs continuous insulation throughout its entire envelope without any thermal bridging.</li>
                        <li>The building envelope is extremely airtight, preventing infiltration of outside air and loss of conditioned air.</li>
                        <li>Employs high-performance windows (typically triple-paned) and doors.</li>
                        <li>Uses some form of balanced heat- and moisture-recovery ventilation and a minimal space conditioning system.</li>
                        <li>Solar gain is managed to exploit the sun's energy for heating purposes in the heating season and to minimize overheating during the cooling season.</li>
                    </ul>

                    <br />
                    <p>Passive building principles can be applied to all building typologies – from single-family homes to multifamily
                        apartment buildings, offices, and skyscrapers. </p>

                    <br />
                    <p>Passive design strategy carefully models and balances a comprehensive set of factors including heat emissions
                        from appliances and occupants to keep the building at comfortable and consistent indoor temperatures throughout
                        the heating and cooling seasons. As a result, passive buildings offer tremendous long-term benefits in addition
                        to energy efficiency: </p>
                    <ul>
                        <li>Superinsulation and airtight construction provide <span className="hyper">unmatched comfort</span> even in extreme weather conditions.</li>
                        <li>Continuous mechanical ventilation of fresh filtered air provides <span className="hyper">superb indoor air quality.</span> </li>
                        <li>A comprehensive systems approach to modeling, design, and construction produces <span className="hyper">extremely resilient buildings.</span> </li>
                        <li>Passive building principles offer the <span className="hyper">best path to Net Zero and Net Positive</span> buildings by minimizing the load that renewables are required to provide. </li>
                    </ul>

                </div>
                <div className="fixed-image-back"></div>

                <div id="solar-energy" className="solar-energy container">
                    <h1>About solar elergy</h1>
                    <p className="selectionShareable">Solar power is energy from the sun that is converted into thermal or electrical&nbsp;energy.</p>
                    <p className="selectionShareable">Solar energy is the cleanest and most abundant renewable energy source available, and the U.S. has some of the richest solar resources in the world. Modern technology can harness this energy for a variety of uses, including generating electricity, providing light or a comfortable interior environment, and heating water for domestic, commercial, or industrial&nbsp;use.</p>
                    <p className="selectionShareable">The U.S. solar market faces both challenges and opportunities; the industry is working to scale up the production of solar technology, and drive down manufacturing and installation&nbsp;costs.</p>
                    <p className="selectionShareable">There are several ways to harness solar energy:&nbsp;<a href="http://www.seia.org/policy/solar-technology/photovoltaic-solar-electric" target="_self">photovoltaics</a>&nbsp;(also called solar electric),&nbsp;<a href="http://www.seia.org/policy/solar-technology/solar-heating-cooling" target="_self">solar heating &amp; cooling</a>,&nbsp;<a href="http://www.seia.org/policy/solar-technology/concentrating-solar-power" target="_self">concentrating solar power</a>&nbsp;(typically built at utility-scale), and passive&nbsp;solar.</p>
                    <p className="selectionShareable">The first three are&nbsp;<em>active&nbsp;</em>solar systems, which use mechanical or electrical devices that convert the sun's heat or light to another form of usable energy.&nbsp;<em>Passive&nbsp;</em>solar buildings are designed and oriented to collect, store, and distribute the heat energy from sunlight to maintain the comfort of the occupants without the use of moving parts or&nbsp;electronics.</p>
                    <p className="selectionShareable"><a href="http://www.seia.org/policy/solar-technology">Solar energy</a>&nbsp;is a flexible energy technology: solar power plants can be built as&nbsp;<a href="http://www.seia.org/policy/distributed-solar" target="_self">distributed generation</a>&nbsp;(located at or near the point of use) or as a&nbsp;<a href="http://www.seia.org/policy/power-plant-development/utility-scale-solar-power" target="_self">central-station, utility-scale solar power plant</a>&nbsp;(similar to traditional power plants). Some utility-scale solar plants can store the energy they produce for use after the sun&nbsp;sets.</p>
                    <img src="https://www.scienceabc.com/wp-content/uploads/2016/06/Solar-irradiance-from-the-sun.jpg"/>
                    <img src="https://www.auroraenergy.com.au/Aurora/media/images/your-home/electricity/how-solar-works.jpg"/>
                </div>
                <div className="register-now container">
                    <h1>
                        Register now
                    </h1>
                    <p>Get registered now and be part of <b>PASSIVE EHOUSE</b> community.</p>
                    <p>get help, support and advices for bulding your own passive house during</p>
                    <p> <b>design, implementation and monitorization phases</b></p>
                    <button onClick={this.goToRegister.bind(this)}>register</button>
                </div>
                <div id="why-passive" className="why-passive container">
                    <h1>
                        Pro and cons for Passive House construction
                    </h1>
                    <div className="half-width-container">
                        <div className="half-width">
                            <img src="../../public/pro_icon.png" className="pro" />
                            <div className="head pro">
                                PROS
                            </div>
                            <ul className="pro">
                                <li>Thicker walls offer more heat retention.</li>
                                <li>Insulation creates a sound barrier for quiet.</li>
                                <li>Results include reduced operational costs.</li>
                                <li>Long-term cost savings using energy efficiency.</li>
                                <li>Systems need minimal space in the design construction.</li>
                                <li>Integrates with hot water heating system.</li>
                            </ul>
                        </div>
                        <div className="half-width">
                            <img src="../../public/con_icon.png" className="con" />
                            <div className="head con">
                                CONS
                            </div>
                            <ul className="con">
                                <li>Initial costs can be 10%-30% higher with new or renovated construction.</li>
                                <li>Weather conditions can alter performance.</li>
                                <li>External conditions make a difference in design.</li>
                                <li>Occupants need to learn how to operate controls.</li>
                                <li>A back-up heating system may be needed.</li>
                                <li>Potential thermal bridging threats exist.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="passive-examples" className="passive-examples last-in-main big-frame">
                    <h1>Passive house examples</h1>
                    <Slider {...settings2}>
                        <div>
                            <iframe src="https://3dwarehouse.sketchup.com/embed.html?mid=uea7937a2-fc59-4ae2-91e4-74392635f71b&width=580&height=326" frameBorder="0" scrolling="no" width="70%" height="100%" allowFullScreen></iframe>
                        </div>
                        <div>
                            <iframe src="https://3dwarehouse.sketchup.com/embed.html?mid=615a40f811448ce92e76371f08487bc7&width=580&height=326" frameBorder="0" scrolling="no" width="70%" height="100%" allowFullScreen></iframe>
                        </div>
                        <div>
                            <iframe src="https://3dwarehouse.sketchup.com/embed.html?mid=dc70064ed7defd013cfe93e1fa6a4623&width=580&height=326" frameBorder="0" scrolling="no" width="70%" height="100%" allowFullScreen></iframe>
                        </div>
                    </Slider>

                </div>
            </div>

        )

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        store: {
            type: state.type,
            dest: state.dest
        }
    }

}

export default connect(mapStateToProps)(Homepage as any);

//export default withRouter(connectedHomepage);
