import * as React from "react"
import * as ReactDOM from 'react-dom';

import "../css/Footer.less"

interface IPropsInterface {
   
}

export class Footer extends React.Component < IPropsInterface,
any > {

    render() {

        return (
            <div className="footer">
                <div className="title">
                    Passive EHouse 
                </div> 
                <div className="image">
                    <img src="../../public/eco-logo.png" />
                </div>
                <div className="project">
                    Project for User Interface Design UTCN @2017
                </div>
            </div>
        );

    }

}