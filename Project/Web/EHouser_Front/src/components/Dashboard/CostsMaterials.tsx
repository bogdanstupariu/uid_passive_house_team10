import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../../store/actions';

import "../../css/App.less"
import "../../css/Dashboard.less"

interface ComponentProps {

}


interface IStateInterface {
    enabled: boolean;
    panelNr: number;
    houseProject: any;
    cost: number;
}


class CostMaterials extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled: false,
            panelNr: 0,
            houseProject: [],
            cost: 0
        }
    }

    componentDidMount() { }


    getCost() {
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/PassiveHouse/GetCosts?passiveHouseId=" + this.state.houseProject.Id + "&noOfSolarPanels=" + this.state.panelNr;
        var request = new Request(requ, {
            method: "GET",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                if (data != null)
                    component.setState({ cost: data })
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, "Cannot compute the cost", "red-alert"));
                component.props.isLoading(false);
            })
    }

    componentDidUpdate() {
        if (this.props.enabled != this.state.enabled)
            this.setState({ enabled: this.props.enabled })
        if (this.props.houseProject != this.state.houseProject)
            this.setState({ houseProject: this.props.houseProject })
    }

    dataBind = (param: any, text: any) => {
        if (param == "panels") {
            this.setState({ panelNr: text.target.value },
                () => {
                    if (this.state.panelNr != 0)
                        this.getCost();
                    else
                        this.setState({cost:0});
                });

        }
    }

    render() {
        return (
            <div className={(this.state.enabled ? "costs-materials full-height" : "hidden")}>
                <div className="half-width-container">
                    <div className="half-width nr-info-cost" >
                        <div style={{ "display": "flex" }}>
                            <div className="compute-cost">
                                <i className="fa fa-circle-o-notch fa-spin fa-3x fa-fw">
                                </i>
                                <textarea onChange={this.dataBind.bind(this, "panels")} placeholder="fill number of solar panels" />
                            </div>
                            <div className="result-cost">
                                {this.state.cost} $
                            </div>
                        </div>
                        <div className="size-info">
                            <h1>Solar panel size</h1>
                            <h3>Typically: <br/>65 inches x 39 inches (1.651 x 0.9906 m)</h3>
                            <h3>SunPower panels: <br/>61.3 inches x 41.2 inches (1.55702 x 1.04648 m)</h3>
                            <i className="fa fa-superpowers" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div className="half-width text-info-cost">
                        <div>
                            <p> Photovoltaic (PV) solar panels (most commonly used in residential installations) come
                                in wattages ranging from about 150 watts to 370 watts per panel, depending on the panel size
                                and efficiency (how well a panel is able to convert sunlight into energy), and on the cell
                                technology.</p>
                            <br />
                            <p>For example, solar cells with no grid lines on the front (like SunPower cells) absorb more sunlight
                                than conventional cells and do not suffer from issues such as delamination (peeling). The construction
                                of our cells make them stronger and more resistant to cracking or corrosion. And a microinverter on each
                                panel can optimize power conversion at the source,
                                in contrast to one large inverter mounted on the side of the house.</p>
                            <br />
                            <p>Typical residential solar panel dimensions today are about 65 inches by 39 inches, or 5.4 feet by 3.25 feet,
                                with some variation among manufacturers. SunPower panels are 61.3 inches by 41.2 inches.</p>
                            <br />
                            <p>
                                These dimensions have remained more or less unchanged for decades, but the efficiency and output from that same
                             footprint have changed dramatically for the better. In addition, SunPower designs entire systems to have virtually
                              no gaps between panels and uses invisible framing and mounting hardware to keep the rooftop footprint as tight,
                               efficient and attractive as possible.
                            </p>
                            <br />
                            <br />
                            <p>In 2018 most homeowners are paying between 2.87$ and 3.85 $ per watt to install solar panels.<br /><br />
                                ⇒	<b>Number of solar panels * number of produced watts * price per watt</b>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(CostMaterials as any);