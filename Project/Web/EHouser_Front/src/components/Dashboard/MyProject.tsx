import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu } from '../../store/actions';

import "../../css/App.less"
import "../../css/Dashboard.less"

interface ComponentProps {
   
}


interface IStateInterface {
   enabled:boolean;
   houseProject:any;
}


class MyProject extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled:false,
            houseProject:[]
        }
    }

    componentDidMount() {}

    componentDidUpdate(){
        if(this.props.enabled != this.state.enabled)
            this.setState({enabled:this.props.enabled})
        if(this.props.houseProject != this.state.houseProject)
            this.setState({houseProject:this.props.houseProject})
    }

    dataBind = (param: any, text: any) => {}

    render() {
        return (
            <div className={(this.state.enabled?"":"hidden")}>
                <div className="my-project">
                    <div className="header-my-project"> 
                        <h1>My project</h1>
                        <img src={this.state.houseProject.Picture}/>
                    </div>
                    <p className="descript">{this.state.houseProject.Description}</p>
                    <div className="half-width-container first-row">
                        <div className="third-width align-center">
                            <i className="fa fa-cube fa-4x" aria-hidden="true"></i>
                            <h2>Number of rooms: {this.state.houseProject.NoOfRooms}</h2>
                        </div>
                        <div className="third-width align-center">
                            <i className="fa fa-map fa-4x" aria-hidden="true"></i>
                            <h2>Area: {this.state.houseProject.Area}</h2>
                        </div>
                        <div className="third-width align-center">
                            <i className="fa fa-cogs fa-4x" aria-hidden="true"></i>
                            <h2>Monthly Consumption: {this.state.houseProject.MonthlyConsumption}</h2>
                        </div>
                    </div>
                    <div className="half-width-container first-row">
                        <div className="third-width align-center">
                            <i className="fa fa-map-marker fa-4x" aria-hidden="true"></i>
                            <h2>Address: {this.state.houseProject.Address}</h2>
                        </div>
                        <div className="third-width align-center">
                            <i className="fa fa-eur fa-4x" aria-hidden="true"></i>
                            <h2>Budget: {this.state.houseProject.Budget}</h2>
                        </div>
                        <div className="third-width align-center">
                            <i className="fa fa-tag fa-4x" aria-hidden="true"></i>
                            <h2>Internal Id: {this.state.houseProject.Id}</h2>
                        </div>
                    </div>
                </div>
                
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(MyProject as any);