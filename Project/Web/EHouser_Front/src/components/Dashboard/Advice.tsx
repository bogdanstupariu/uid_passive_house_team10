import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../../store/actions';

import "../../css/App.less"
import "../../css/Advice.less"

interface ComponentProps {

}


interface IStateInterface {
    text: string;
    id: string;
    allResponses: any;
    showLarge: boolean;
    isSpecialist: boolean;
    respText: string;
    diplomas: any;
    isSpecialistVerified: boolean;
    isAdmin: boolean;
}


class Advice extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            text: "",
            id: "",
            allResponses: [],
            showLarge: false,
            isSpecialist: false,
            respText: "",
            diplomas: [],
            isSpecialistVerified: false,
            isAdmin: false
        }

    }

    componentDidMount() {

    }

    componentDidUpdate() {
        if (this.state.text != this.props.text)
            this.setState({ text: this.props.text });
        if (this.state.id != this.props.id) {
            this.setState({ id: this.props.id }, () => {
                if (this.props.id != "") {
                    this.getAllResponses();
                }
            });
        }
        if (this.state.isSpecialist != this.props.isSpecialist)
            this.setState({ isSpecialist: this.props.isSpecialist });
        if (this.state.isAdmin != this.props.isAdmin)
            this.setState({ isAdmin: this.props.isAdmin });
        if (this.state.diplomas != this.props.diplomas) {
            this.setState({ diplomas: this.props.diplomas });
            if (this.props.diplomas != null) {
                var len = this.props.diplomas.length;
                if (len !== 0) {
                    this.props.diplomas.forEach((element: any) => {
                        if (element.Verified == true)
                            this.setState({ isSpecialistVerified: true });
                    });
                }
            }
        }
    }

    getAllResponses() {
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/OffersAndAdvicesResponse/GetResponsesForSpecificRequest?requestId=" + this.state.id;
        var request = new Request(requ, {
            method: "GET",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                component.setState({ allResponses: data }, () => {
                    var i = 0;
                    data.forEach((element: any) => {
                        component.setUserName(element, i);
                        i = i + 1;
                    });


                });
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    setUserName(resp: any, i: number) {
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/Account/GetUserById?userUri=" + resp.UserId;
        var request = new Request(requ, {
            method: "GET",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                const allReq = component.state.allResponses;
                allReq[i].UserId = data.UserName;
                component.setState({ allResponses: allReq });
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    addResponse() {
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        myheader.set('Content-Type', 'application/x-www-form-urlencoded');
        var component = this;
        var requ = "http://localhost:2766/api/OffersAndAdvicesResponse/AddOffersAndAdvicesResponse";
        var request = new Request(requ, {
            method: "POST",
            headers: myheader,
            body: "RequestId=" + this.state.id + "&Description=" + this.state.respText
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                var resp = component.state.allResponses;
                resp.push(data);
                component.setState({ allResponses: resp });
                window.location.reload();
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    dataBind = (param: any, text: any) => {
        if (param == "show" && this.state.showLarge == false) {
            this.setState({ showLarge: true });
        }
        if (param == "hideLarge") {
            this.setState({ showLarge: false });
        }
        if (param == "textUpdate") {
            this.setState({ respText: text.target.value });
        }
        if (param == "addResp") {
            if (this.state.respText == "") {
                this.props.dispatch(showAlert(true, "Text field cannot be empty", "red-alert"));
            } else {
                this.addResponse();
            }
        }
    }

    render() {
        return (
            <div className={"advice " + (this.state.showLarge ? "item1" : "")} onClick={this.dataBind.bind(this, "show")}>
                <div className="icon-holder">
                    <i className="fa fa-info-circle fa-3x" aria-hidden="true"></i>
                </div>
                <div className="info-holder">
                    <h3>Advice  #{this.state.id}</h3>
                    <div className="question">
                        {this.state.text}
                    </div>
                    <div className="resp_nr">number of responses: {this.state.allResponses.length}</div>
                    <div className={"responses " + (this.state.showLarge ? "" : "hidden")}>

                        <i className="fa fa-window-close" onClick={this.dataBind.bind(this, "hideLarge")} aria-hidden="true"></i>

                        {this.state.allResponses.length == 0 ? "No responses yet" : ""}
                        {this
                            .state
                            .allResponses
                            .map((event: any, i: any) => {
                                return (
                                    <div className="response" key={i}>
                                        <div className="icon-holder">
                                            <i className="fa fa-user-circle fa-2x" aria-hidden="true"></i>

                                        </div>
                                        <div className="info-holder">
                                            <b style={{ "display": "block" }}> {event.UserId}</b>
                                            {event.Description}
                                        </div>

                                    </div>
                                )
                            })
                        }
                        {this.state.isSpecialist
                            ? (

                                <div>
                                    {
                                        this.state.isSpecialistVerified ? (
                                            <div  className="specialist-input-advice">
                                                <input onChange={this.dataBind.bind(this, "textUpdate")} />
                                                <button onClick={this.dataBind.bind(this, "addResp")}>Add response</button>
                                            </div>
                                        )
                                            : (
                                                <div>
                                                    <h3>This specialist account is not yet verified. Add a diploma in order to get your account verified</h3>
                                                    <h4>You cannot give advices until your account gets verified</h4>
                                                </div>
                                            )

                                    }
                                </div>

                            )
                            : ""}
                    </div>
                </div>

            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(Advice as any);