import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu } from '../../store/actions';
import Slider from 'react-slick';

import "../../css/App.less"
import "../../css/DashHome.less"

interface ComponentProps {
   
}


interface IStateInterface {
    enabled:boolean;
    width:number;
    page:number;
}


class DashHome extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled:false,
            width:0,
            page:1
        }
    }

    componentDidMount() {}

    componentDidUpdate(){
        if(this.props.enabled != this.state.enabled)
            this.setState({enabled:this.props.enabled})
        if((this.props.width-300) != (this.state.width)){
            this.setState({width:(this.props.width - 300)})
        }
    }

    dataBind = (param: any, text: any) => {
        if(param == "next"){
            var t = this.state.page;
            t++;
            if(t == 11) t=1;
            this.setState({page:t});
        } 
        if(param == "prev"){
            var t = this.state.page;
            t--;
            if(t == 0) t=10;
            this.setState({page:t});
        }
    }

    render() {
        return (
            <div className={(this.state.enabled?"dashhome":"dashhome hidden")}
                style={{"width":this.state.width}}>
                <div className="slider">
                    <div className={(this.state.page == 1 ? "" : "not-visible")}>
                        <h1>1. Development plan </h1>
                        <ul>
                            <li>Good liublic transliort connections</li>
                            <li>South-oriented main facade (± 30°) and large south-facing window areas</li>
                            <li>Avoid shading in order to utilise liassive solar energy</li>
                            <li>Shade-free vegetation</li>
                            <li>Are comliact building shalies liossible? Buildings in rows are advantageous.</li>
                        </ul>
                    </div>
                    <div className={(this.state.page == 2 ? "" : "not-visible")}>
                        <h1> 2. Preliminary planning</h1>
                        <ul>
                            <li>Comliact building structure; avail of the liossibilities for building extensions</li>
                            <li>Glazing areas facing south are olitimal, windows facing east/west/north should be kelit small.</li>
                            <li>Avoid shading (no or minimal shading in winter from liaraliets, liorches, balconies, roof overhangs, liartitions etc.)</li>
                            <li>Simlile structure of envelolie surface (lireferably without dormers or recesses etc.)</li>
                            <li>Layout: keeli installation zones together (e.g. bathroom over or next to kitchen), taking into account necessary ventilation ducts</li>
                            <li>Seliaration of any existing basement level: airtight and free of thermal bridges</li>
                            <li>Alililication for liassive House certification to the liassive House Institute or another certifier accredited by the liHI.</li>
                            <li>Check out and alilily for liassive Houses subsidies by the Develoliment Loan Corlioration (KfW), for examlile</li>
                        </ul>
                    </div>
                    <div className={(this.state.page == 3 ? "" : "not-visible")}>
                    <h1>3. Planning approval </h1>
                        <ul>
                            <li>Include planning of the insulation thicknesses for the building envelope</li>
                            <li>Avoid thermal bridges</li>
                            <li>Include planning of building services</li>
                            <li>Layout: short pipes (for hot water, cold water, waste water) and short ventilation ducts</li>
                            <li>Cold air ducts outside of the envelope; warm ducts within the envelope</li>
                        </ul>
                    </div>
                    <div className={(this.state.page == 4 ? "" : "not-visible")}>
                    <h1>4. Implementation planning of the building structure</h1>
                        <ul>
                            <li>Highly insulated standard constructions (standard: U ≤ 0,15 W/(m²K); aim for U = 0,1 W/(m²K))</li>
                            <li>Thermal bridge free connection details: calculation or thermal bridge free design</li>
                            <li>⇒ Airtight connection details</li>
                            <li>Window optimisation (type of glazing, superior frame, glazing ratio, solar protection)</li>
                            <li>⇒ Calculation of the specific energy demand; use the Passive House Planning Package (PHPP)</li>
                        </ul>
                    </div>
                    <div className={(this.state.page == 5 ? "" : "not-visible")}>
                    <h1>5.  Implementation planning of the ventilation system</h1>
                    <ul>
                        <li>Post-heating coils inside the warm envelope</li>
                        <li>Possibly additional insulation of central unit and post-heating coils</li>
                        <li>Heat recovery rate ≥ 75%; airtight (recirculated air smaller than 3%); electrical efficiency (smaller than 0,4 Wh/m3)</li>
                        <li>Adjustability; acoustic insulation; highly insulated device casing</li>
                        <li>Adjustment of the ventilation system: User-controlled “low”, “normal”, “high”; Possibly additional switch in kitchens, bathrooms and toilets for use when required.</li>
                        <li>Extractor hoods: high rate of extraction with low volumetric flow; grease filter</li>
                        <li>Subsoil heat exchanger optionally</li>
                    
                    </ul>
                    </div>
                    <div className={(this.state.page == 6 ? "" : "not-visible")}>
                    <h1>6. Implementation planning of remaining building services</h1>
                    <ul>
                        <li>Plumbing and hot water: short pipes, well-insulated, within the envelope</li>
                        <li>Plumbing, cold water: short pipes, normally insulated against surface condensation</li>
                        <li>Insulate fittings for hot water and heating</li>
                        <li>Water-saving taps; hot water connections at washing machines and dishwashers</li>
                        <li>Waste water: short pipes (only one downpipe), roof vents (preferable) or insulated vent pipe</li>
                        <li>Use energy-saving household appliances</li>
                        <li>Carry out quality control for the entire building services</li>
                    
                    </ul>
                    </div>
                    <div className={(this.state.page == 7 ? "" : "not-visible")}>
                    <h1>7. Execution, construction management for building structure</h1>
                    <ul>
                        <li>⇒ Absence of thermal bridges: Schedule for quality assurance on site</li>
                        <li>⇒ Insulation work: Uninterrupted insulation layers; avoid hollow spaces</li>
                        <li>⇒ Airtightness: Check connection details as long as they remain accessible</li>
                        <li>⇒ Airtightness: Carry out the airtightness test during the construction period!</li>
                        <li>When? As soon as the airtight envelope has been formed but is still accessible, i.e. before interior finishing (coordination with other disciplines!)</li>
                        <li>How? n50 test using e.g. BlowerDoor; including leak detection</li>
                    </ul>
                    </div>
                    <div className={(this.state.page == 8 ? "" : "not-visible")}>
                    <h1>8. Execution, construction management for ventilation system</h1>
                    <ul>
                        <li>⇒ Penetrations: airtight</li>
                        <li>Ducts: install these in a clean state, seal carefully</li>
                        <li>Central unit: accessibility of the filter for replacement; acoustic insulation</li>
                        <li>Check insulation of ducts (where necessary)</li>
                        <li>⇒ Calibration of the air flows during normal operation</li>
                        <li>Measurement of supply air and extract air flows; adjustment of balance; </li>
                    </ul>
                    </div>
                    <div className={(this.state.page == 9 ? "" : "not-visible")}>
                    <h1>9. Execution, construction management for remaining building services</h1>
                    <ul>
                        <li>⇒ Check: airtight penetrations</li>
                        <li>⇒ Check: thermal insulation of pipes</li>
                    </ul>
                    </div>
                    <div className={(this.state.page == 10 ? "" : "not-visible")}>
                    <h1>10. Certificate</h1>
                    <ul>
                        <li>Receive the certificate and certification booklet for your ”Certified Passive House”</li>
                        <li>Install the "Certified Passive House" wall plaque</li>
                        <li>Participate at the International Passive House Days</li>
                    </ul>
                    </div>
                </div>
                <div className="slider-btn">
                    <button onClick={this.dataBind.bind(this,"prev")}>Previous step</button>
                    <button onClick={this.dataBind.bind(this,"next")}>Next step</button>
                </div>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(DashHome as any);