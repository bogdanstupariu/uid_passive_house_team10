import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../../store/actions';

import "../../css/App.less"
import "../../css/Dashboard.less"
import Diploma from "./Diploma";

interface ComponentProps {

}


interface IStateInterface {
    enabled: boolean;
    specialistDiplomas: any;
    newPath: string;
    isAdmin: boolean;
}


class Diplomas extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled: false,
            specialistDiplomas: [],
            newPath: "",
            isAdmin: false
        }
    }

    componentDidMount() { }

    componentDidUpdate() {
        var __this = this;
        if (this.props.enabled != this.state.enabled)
            this.setState({ enabled: this.props.enabled })
        if (this.props.isAdmin != this.state.isAdmin)
            this.setState({ isAdmin: this.props.isAdmin })
        if (this.props.specialistDiplomas != this.state.specialistDiplomas) 
            this.setState({ specialistDiplomas: this.props.specialistDiplomas })
    }

    dataBind = (param: any, text: any) => {
        if (param = "path") {
            this.setState({ newPath: text.target.value });
        }
    }

    addDiploma() {
        if (this.state.newPath != "") {
            var auth_token = localStorage.getItem("access_token")
            var myheader = new Headers();
            myheader.set('Access-Control-Allow-Origin', '*')
            myheader.set('Authorization', "Bearer " + auth_token);
            var component = this;
            var request = new Request("http://localhost:2766/api/Diploma/AddDiploma?diplomaPath=" + this.state.newPath, {
                method: "POST",
                headers: myheader
            });
            let response = fetch(request).then(function (response) {

                if (response.ok) {
                    return response.json();
                }
                throw new Error('Network response was not ok.');
            })
                .then(function (data) {
                    window.location.reload();
                })
                .catch(function (error) {
                    component.props.dispatch(showAlert(true, error.Message, "red-alert"));
                })
        } else {
            this.props.dispatch(showAlert(true, "Path to diploma should be provided", "red-alert"));
        }
    }

    render() {
        return (
            <div className={(this.state.enabled ? "" : "hidden")}>
                {this.state.isAdmin
                    ? <h2>All diplomas</h2>
                    : <h2>All my diplomas</h2>
                }
                {this.state.specialistDiplomas != null ? (
                    this.state.specialistDiplomas.map((event: any, i: any) => {
                        return (
                            <Diploma {...this.props} key={i} id={event.Id} path={event.DiplomaPath}
                                verified={event.Verified} isAdmin={this.state.isAdmin} user={event.UserId}/>
                        )
                    })
                )
                    : ""
                }
                {this.state.isAdmin
                    ? ""
                    : <div>
                        <h2>Add new diploma</h2>
                        <div className="add-new-diploma">
                            <input placeholder="path to diploma image/file" onChange={this.dataBind.bind(this, "path")} />
                            <button onClick={this.addDiploma.bind(this)}>Add diploma</button>
                        </div>
                    </div>
                }
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(Diplomas as any);