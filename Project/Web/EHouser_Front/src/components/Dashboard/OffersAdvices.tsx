import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../../store/actions';

import "../../css/App.less"
import "../../css/OffersAdvices.less"
import Advice from "./Advice";

interface ComponentProps {

}


interface IStateInterface {
    enabled: boolean;
    role: {
        user: boolean,
        admin: boolean,
        specialist: boolean
    };
    userUri: string;
    allAdvices: any;
    createText:string;
    houseId :number;
    specialistDiplomas: any;
}


class OffersAdvices extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled: false,
            role: {
                user: false,
                admin: false,
                specialist: false
            },
            userUri: "",
            allAdvices: [],
            createText:"",
            houseId:1,
            specialistDiplomas:[]
        }
    }

    componentDidMount() {

    }

    componentDidUpdate() {
        if (this.props.enabled != this.state.enabled) {
            this.setState({ enabled: this.props.enabled })
        }
        var type = {
            user: false,
            admin: false,
            specialist: false
        }
        var changed = false;
        if (this.props.role.user != this.state.role.user) {
            type.user = this.props.role.user;
            changed = true;
        }
        if (this.props.role.admin != this.state.role.admin) {
            type.admin = this.props.role.admin;
            changed = true;
        }
        if (this.props.role.specialist != this.state.role.specialist) {
            type.specialist = this.props.role.specialist;
            changed = true;
        }
        if (changed) this.setState({ role: type });

        if (this.state.userUri != this.props.userUri)
            this.setState({ userUri: this.props.userUri }, () => { 
                if(this.state.role.specialist || this.state.role.admin){
                    this.getAllAdvicesForAllUsers() 
                } else
                    this.getAllAdvices() 
            });

        if(this.state.specialistDiplomas != this.props.specialistDiplomas){
            this.setState({specialistDiplomas: this.props.specialistDiplomas});
        }
    }

    getAllAdvicesForAllUsers(){
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/OffersAndAdvicesRequest/GetAllRequests";
        var request = new Request(requ, {
            method: "GET",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                component.setState({ allAdvices: data });
            })
            .then(function () {
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    getAllAdvices() {
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/OffersAndAdvicesRequest/GetRequestsOfSpecificUser?userId=" + this.state.userUri;
        var request = new Request(requ, {
            method: "GET",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                component.setState({ allAdvices: data });
            })
            .then(function () {
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    createAdvice(){
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        myheader.set('Content-Type','application/x-www-form-urlencoded');
        var component = this;
        var requ = "http://localhost:2766/api/OffersAndAdvicesRequest/AddOffersAndAdvicesRequest";
        var request = new Request(requ, {
            method: "POST",
            headers: myheader,
            body: "PassiveHouseId="+this.state.houseId+"&Description="+this.state.createText
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                window.location.reload();
            })
            .then(function () {
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, error.Message, "red-alert"));
            })
    }

    dataBind = (param: any, text: any) => {
        if(param == "create"){
            if(this.state.createText == ""){
                this.props.dispatch(showAlert(true, "Text field cannot be empty", "red-alert"));
            } else {
                this.createAdvice();
            }
        }
        if(param == "text"){
            this.setState({createText:text.target.value});
        }
     }


    render() {
        return (
            <div className={(this.state.enabled ? "" : "hidden")}>
                <div className={(this.state.role.user ? "hidden" : "")}>
                     <div className="full-width"><h2>All advices and offers</h2></div>
                    <div className="user-add-advice">
                        {this
                            .state
                            .allAdvices
                            .map((event: any, i: any) => {
                                return (
                                    <Advice {...this.props} key={i} text={event.Description} id={event.Id} isAdmin={this.state.role.admin}
                                    isSpecialist={this.state.role.specialist} diplomas={this.state.specialistDiplomas}/>
                                )
                            })
                        }
                    </div>
                </div>
                <div className={(this.state.role.user ? "advice-panel" : "advice-panel hidden")}>
                    <div className="full-width"><h2>My advices and offers</h2></div>
                        <div className="user-add-advice">
                            {this.state.allAdvices.map((event: any, i: any) => {
                                    return (
                                        <Advice {...this.props} key={i} text={event.Description} id={event.Id} />
                                    )
                                })
                            }
                    </div>
                    <div className="full-width"><h2>Request new advice or offer</h2></div>
                    <div className="full-width add-advice">
                        <textarea onChange={this.dataBind.bind(this,"text")}/>
                        <button onClick={this.dataBind.bind(this,"create")}>Create</button>
                    </div>
                    
                </div>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(OffersAdvices as any);