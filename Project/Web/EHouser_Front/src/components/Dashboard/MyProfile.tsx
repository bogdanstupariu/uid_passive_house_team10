import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu } from '../../store/actions';

import "../../css/App.less"
import "../../css/Profile.less"

interface ComponentProps {
   
}


interface IStateInterface {
    enabled:boolean;
    profile:any;
    isSpecialist:boolean;
}


class MyProfile extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled:false,
            profile:[],
            isSpecialist:false
        }
    }

    componentDidMount() {

    }

    componentDidUpdate(){
        if(this.props.enabled != this.state.enabled)
            this.setState({enabled:this.props.enabled})
        if(this.props.userProfile != this.state.profile)
            this.setState({profile:this.props.userProfile})
        if(this.props.isSpecialist != this.state.isSpecialist)
            this.setState({isSpecialist:this.props.isSpecialist})
    }

    dataBind = (param: any, text: any) => {}

    render() {
        return (
            <div className={(this.state.enabled?"":"hidden")}>
                <div>
                    <i className="fa fa-address-card-o my-fa-add" aria-hidden="true"> <h1>User profile</h1></i>
                </div>
                <div style={{"display":"flex"}}>
                    <div>
                        <h2>FirstName</h2>
                        <h2>LastName</h2>
                        <h2>Email</h2>
                        <h2>PhoneNumber</h2>
                        <h2>Specialist</h2>
                    </div>
                    {(this.state.profile != null)
                        ?(
                    <div className="responded" style={{"marginLeft": "65px"}}>
                      
                            <h2>{this.state.profile.FirstName}</h2>
                            <h2>{this.state.profile.LastName}</h2>
                            <h2>{this.state.profile.Email}</h2>
                            <h2>{this.state.profile.PhoneNumber}</h2>
                            <h2>{this.state.isSpecialist.toString()}</h2>
                       
                    </div>
                     )
                     :""}

                </div>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(MyProfile as any);