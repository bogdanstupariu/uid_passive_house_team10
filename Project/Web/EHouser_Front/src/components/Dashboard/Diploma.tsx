import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../../store/actions';

import "../../css/App.less"
import "../../css/Dashboard.less"

interface ComponentProps {

}


interface IStateInterface {
    enabled: boolean;
    id: string;
    user: string;
    path: string;
    verified: boolean;
    isAdmin: boolean;
    userName:string;
}


class Diploma extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled: false,
            id: "",
            user: "",
            path: "",
            verified: false,
            isAdmin: false,
            userName:""
        }
    }

    componentWillMount() {
    }

    componentDidMount() { }

    componentDidUpdate() {
        if (this.props.enabled != this.state.enabled)
            this.setState({ enabled: this.props.enabled })
        if (this.props.isAdmin != this.state.isAdmin)
            this.setState({ isAdmin: this.props.isAdmin })
        if (this.props.id != this.state.id)
            this.setState({ id: this.props.id })
        if (this.props.verified != this.state.verified) 
            this.setState({ verified: this.props.verified })
        if (this.props.user != this.state.user){
            this.setState({ user: this.props.user }, ()=>{this.getUserByUri()})
            
        }
        if (this.props.path != this.state.path)
            this.setState({ path: this.props.path })
    }

    getUserByUri(){
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/Account/GetUserById?userUri=" + this.state.user;
        var request = new Request(requ, {
            method: "GET",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                if(data != null)
                component.setState({ userName: data.UserName });
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, "Error in diploma call get user by uri", "red-alert"));
            })
    }

    markVerified(){
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/Diploma/MarkDiplomaAsVerified?id=" + this.state.id;
        var request = new Request(requ, {
            method: "POST",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                window.location.reload();
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, "Error in diploma call get user by uri", "red-alert"));
            })
    }

    dataBind = (param: any, text: any) => { }

    render() {
        return (
            <div className="diploma">
                <div >
                    <i className="fa fa-address-card-o fa-3x" aria-hidden="true"></i>
                </div>
                <div >

                    <div style={{ "display": "flex" }}>
                        <div className="column left">
                            {this.state.userName}
                        </div>
                        <div className="column center">
                            #id: {this.state.id}
                        </div>
                        <div className="column right">
                            verified: {this.state.verified.toString()}
                        </div>
                    </div>
                    <div style={{ "maxWidth": "84%", "wordBreak": "break-all", "fontSize": "13px" }}>
                        <a href={this.state.path} target="blank">{this.state.path}</a>
                    </div>


                </div>

                {this.state.verified
                    ? <i className="fa fa-check-circle fa-3x" aria-hidden="true"></i>
                    :
                    <div>
                        {this.state.isAdmin
                            ? <div className="btn-check-verified">
                                <button onClick={this.markVerified.bind(this)}>Mark as verified</button>
                            </div>
                            : ""}
                        <i className="fa fa-times-circle fa-3x" aria-hidden="true"></i>
                    </div>
                }
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(Diploma as any);