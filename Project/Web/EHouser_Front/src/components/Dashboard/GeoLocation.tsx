import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../../store/actions';

import "../../css/App.less"
import "../../css/Dashboard.less"

interface ComponentProps {
   
}


interface IStateInterface {
    enabled:boolean;
    fullScreen:boolean;
    energy:number;
    geoL:number;
    houseProject:any;
}


class GeoLocation extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled:false,
            fullScreen:false,
            energy:0,
            geoL:0,
            houseProject:[]
        }
    }

    componentDidMount() {}

    componentDidUpdate(){
        if(this.props.enabled != this.state.enabled)
            this.setState({enabled:this.props.enabled})
        if (this.props.houseProject != this.state.houseProject)
            this.setState({ houseProject: this.props.houseProject })
    }

    getCost(){
        var auth_token = localStorage.getItem("access_token")
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')
        myheader.set('Authorization', "Bearer " + auth_token);
        var component = this;
        var requ = "http://localhost:2766/api/PassiveHouse/GetHourlyEnergyBasedOnLocation?passiveHouseId=" + this.state.houseProject.Id + "&d=" + this.state.geoL;
        var request = new Request(requ, {
            method: "GET",
            headers: myheader
        });

        this.props.isLoading(true);
        let response = fetch(request).then(function (response) {

            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                if (data != null)
                    component.setState({ energy: data })
                component.props.isLoading(false);
            })
            .catch(function (error) {
                component.props.dispatch(showAlert(true, "Cannot compute the cost", "red-alert"));
                component.props.isLoading(false);
            })
    }

    dataBind = (param: any, text: any) => {
        if(param == "full")
            this.setState({fullScreen:true});
        if(param == "nofull")
            this.setState({fullScreen:false});
            if (param == "panels") {
                this.setState({ geoL: text.target.value },
                    () => {
                        if (this.state.geoL != 0)
                            this.getCost();
                        else
                            this.setState({energy:0});
                    });
    
            }
    }

    render() {
        return (
            <div className={(this.state.enabled?"":"hidden")} style={{"display":"flex"}}>
                <div  className={this.state.fullScreen?"full-screen":""}>
                    <img src="../../../public/map.jpg" onClick={this.dataBind.bind(this,"full")}/>
                    {this.state.fullScreen?
                    <i onClick={this.dataBind.bind(this,"nofull")} className="fa fa-window-close" aria-hidden="true"></i>
                    :""}
                </div>
                <div className="compute-map">
                    <i className="fa fa-sun-o fa-spin" aria-hidden="true"></i>
                    <textarea onChange={this.dataBind.bind(this, "panels")} placeholder="index" />
                    <h1>{this.state.energy.toFixed()} W</h1>        
                </div>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(GeoLocation as any);