import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu, showAlert } from '../../store/actions';

import "../../css/App.less"
import "../../css/Dashboard.less"
import { InputDesigned } from '../Utility/InputDesigned';

interface ComponentProps {
   
}


interface IStateInterface {
    enabled:boolean;
    NoOfRooms:number;
    Area:number;
    MonthlyConsumption:number;
    Description:string,
    Address:string,
    Picture:string,
    Budget:string
}


class CreateProject extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled:false,
            NoOfRooms:0,
            Area:0,
            MonthlyConsumption:0,
            Description:"",
            Address:"",
            Picture:"",
            Budget:""
        }
    }

    componentDidMount() {}

    componentDidUpdate(){
        if(this.props.enabled != this.state.enabled)
            this.setState({enabled:this.props.enabled})
    }

    dataBind = (param: any, text: any) => {
        switch(param){
            case "NoOfRooms":
            this.setState({NoOfRooms:text})
            break
            case "Area":
            this.setState({Area:text})
            break
            case "MonthlyConsumption":
            this.setState({MonthlyConsumption:text})
            break
            case "Address":
            this.setState({Address:text})
            break
            case "Description":
            this.setState({Description:text.target.value})
            break
            case "Picture":
            this.setState({Picture:text})
            break
            case "Budget":
            this.setState({Budget:text})
            break
        }
    }

    verifyInput(){
        if(this.state.Address == "" || this.state.MonthlyConsumption == 0 ||
            this.state.Area == 0 ||  this.state.NoOfRooms == 0 || this.state.Picture == "" ||
            this.state.Budget == "" || this.state.Description == "")
            return false;
        return true;
    }

    createProject(){
        if(this.verifyInput()){
            var auth_token = localStorage.getItem("access_token")
            var myheader = new Headers();
            myheader.set('Access-Control-Allow-Origin', '*')
            myheader.set('Authorization', "Bearer " + auth_token);
            myheader.set('Content-Type','application/x-www-form-urlencoded');
            var component = this;
            var request = new Request("http://localhost:2766/api/PassiveHouse/AddPassiveHouse", {
                method: "POST",
                headers: myheader,
                body: "NoOfRooms=" + this.state.NoOfRooms + 
                        "&Area=" + this.state.Area + 
                        "&MonthlyConsumption=" + this.state.MonthlyConsumption +
                        "&Address=" + this.state.Address +
                        "&Budget=" + this.state.Budget +
                        "&Picture=" + this.state.Picture +
                        "&Description=" + this.state.Description
            });
            let response = fetch(request).then(function (response) {

                if (response.ok) {
                    return response.json();
                }
                throw new Error('Network response was not ok.');
            })
                .then(function (data) {
                    window.location.reload();
                })
                .catch(function (error) {
                    component.props.dispatch(showAlert(true, error.Message, "red-alert"));
                })
        } else {
            this.props.dispatch(showAlert(true, "Please fill in the required fields", "red-alert"));
        }
    }

    render() {
        return (
            <div className={(this.state.enabled?"create-proj":"hidden")}>
                <div className="header-create">
                    <h1>Create your house project</h1>
                    
                </div>
                <div style={{"display":"flex"}}>
                    <div className="content-create">
                        <InputDesigned  placeholder="Number of rooms"
                            type="number"
                            glyph="glyphicon-lock"
                            isValid={true}
                            data={this.dataBind.bind(this,"NoOfRooms")}/>
                        <InputDesigned  placeholder="House area"
                            type="number"
                            glyph="glyphicon-lock"
                            isValid={true}
                            data={this.dataBind.bind(this,"Area")}/>
                        <InputDesigned  placeholder="Monthly energy consumption"
                            type="number"
                            glyph="glyphicon-lock"
                            isValid={true}
                            data={this.dataBind.bind(this,"MonthlyConsumption")}/>
                    
                        <InputDesigned  placeholder="Country and city"
                            type="text"
                            glyph="glyphicon-lock"
                            isValid={true}
                            data={this.dataBind.bind(this,"Address")}/>
                        <InputDesigned  placeholder="Picture"
                            type="text"
                            glyph="glyphicon-lock"
                            isValid={true}
                            data={this.dataBind.bind(this,"Picture")}/>
                        <InputDesigned  placeholder="Budget"
                            type="text"
                            glyph="glyphicon-lock"
                            isValid={true}
                            data={this.dataBind.bind(this,"Budget")}/>
                    </div>
                    <div className="right-panel">
                        <img src="../../../public/eco_house_logo.png"/>
                        <textarea  placeholder="Description"
                                onChange={this.dataBind.bind(this,"Description")}/>
                    </div>
                </div>
                <button onClick={this.createProject.bind(this)}>Create project</button>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(CreateProject as any);

