import * as React from "react"
import * as ReactDOM from 'react-dom';

import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { scrollIntoPage, showMenu, hideMenu } from '../../store/actions';

import "../../css/App.less"
import "../../css/Dashboard.less"

interface ComponentProps {
   
}


interface IStateInterface {
    enabled:boolean;
    allHouseProjects:any;
}


class AdminPage extends React.Component<any,
    IStateInterface> {
    constructor(props: any) {
        super(props);
        this.state = {
            enabled:false,
            allHouseProjects:[]
        }
    }

    componentDidMount() {}

    componentDidUpdate(){
        if(this.props.enabled != this.state.enabled)
            this.setState({enabled:this.props.enabled})
        if(this.props.allHouseProjects != this.state.allHouseProjects)
            this.setState({allHouseProjects:this.props.allHouseProjects})
    }

    dataBind = (param: any, text: any) => {}

    render() {
        return (
            <div className={(this.state.enabled?"":"hidden")}>
                 <div className="proj-card-container">
                        {this
                            .state
                            .allHouseProjects
                            .map((event: any, i: any) => {
                                return (
                                    <div className="proj-card" key={i}>
                                        <div className="proj-card-img" style={{"backgroundImage":"url("+event.Picture+")"}}></div>
                                        <div className="proj-card-info">
                                            <h3>Address: {event.Address}</h3>
                                            <h3>Budget: {event.Budget}</h3>
                                            <h3>Area: {event.Area}</h3>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
            </div>
        );

    }

}

function mapStateToProps(state: any, history: any) {
    return {
        scrollInto: state.scrollInto
    };
}

export default connect(mapStateToProps)(AdminPage as any);