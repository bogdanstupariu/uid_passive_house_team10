import 'whatwg-fetch';

import {LoginObject,RegisterObject} from '../components/Login/LoginObject';
import Login from '../components/Login/Login'
import { showAlert } from '../store/actions';

export class LoginServices {

    static API = "http://localhost:2766/";

    static tryToLog(login : any,lg : LoginObject) {
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*')

        var params:any = {
            userName: lg.email.value,
            password: lg.password.value,
            grant_type: 'password'
        };
        
        var formData = new FormData();
        
        for (var k in params) {
            formData.append(k, params[k]);
        }

        var request = new Request(this.API + "token", {
            method: "POST",
            headers: myheader,
            body: "username="+lg.email.value+"&password="+lg.password.value+"&grant_type=password"
        });

        let response = fetch(request).then(function (response) {
         
            if (response.ok) {
                return response.json();
            }
            throw new Error('Network response was not ok.');
        })
            .then(function (data) {
                localStorage.setItem("access_token", data.access_token);
                localStorage.setItem("userName", data.userName);
                login.setState({isLogged: true});
                login.loginSucceeded();
            })
            .catch(function (error) {
                login.props.dispatch(showAlert(true,"Please log in with a valid account","red-alert"));
            })    
    }   

    static insertUser(registerPage : any, regObj : RegisterObject) {
        var myheader = new Headers();
        myheader.set('Access-Control-Allow-Origin', '*');
        myheader.set('Content-Type','application/x-www-form-urlencoded');

        var role = "CommonUser";
        if(registerPage.state.regAsSpec)
            role = "Specialist";

        var request = new Request(this.API + "api/Account/Register", {
            method: "POST",
            headers: myheader,
            body: 
                "Email="+ regObj.email.value+
                "&Password="+ regObj.password.value+
                "&ConfirmPassword="+ regObj.passwordRepeat.value+
                "&FirstName="+ regObj.firstname.value+
                "&LastName="+ regObj.lastname.value+
                "&PhoneNumber="+ regObj.phoneNumber.value+
                "&Role="+ role
            }
        );
        fetch(request).then(function (response) {
            if (response.ok) {
                return response.json();
            } else {
                return response.json()
                .then(function(err) {
                    throw new Error(err.ModelState[""][0]);
                });
            }
          
        })
            .then(function (data) {
                registerPage.registerSuccess();
            })
            .catch(function (error) {
                registerPage
                    .props
                    .dispatch(showAlert(true, error.message, "red-alert"));
            })
    }
}
