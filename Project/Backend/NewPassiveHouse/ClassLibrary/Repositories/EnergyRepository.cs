﻿using ClassLibrary.DAOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repositories
{
    public class EnergyRepository
    {
        public EnergyDAO ConvertToDAO(Energy energy)
        {
            return new EnergyDAO { Id = energy.Id, Type = energy.Type, Value = energy.Value };
        }

        public EnergyDAO[] ConvertToDAOs(Energy[] es)
        {
            EnergyDAO[] esDAO = new EnergyDAO[es.Length];
            for (int i = 0; i < es.Length; i++)
            {
                esDAO[i] = ConvertToDAO(es[i]);
            }
            return esDAO;
        }

        public EnergyDAO[] GetAll()
        {
            return ConvertToDAOs(new NewPassiveHouseEntities().Energies.ToArray());
        }

    }
}
