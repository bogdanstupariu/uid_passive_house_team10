﻿using ClassLibrary;
using ClassLibrary.DAOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repositories
{
    public class SpecialistsDiplomaRepository
    {

        private SpecialitsDiploma ConvertDAOToSpecialitsDiploma(SpecialistsDiplomaDAO specialistsDiplomaDAO)
        {
            return new SpecialitsDiploma { Id = specialistsDiplomaDAO.Id, IdUser = specialistsDiplomaDAO.UserId, DiplomaPath = specialistsDiplomaDAO.DiplomaPath, Verified = specialistsDiplomaDAO.Verified};
        }

        private SpecialistsDiplomaDAO ConvertSpecialistsDiplomaToDAO(SpecialitsDiploma specialistsDiploma)
        {
            return new SpecialistsDiplomaDAO { Id = specialistsDiploma.Id, UserId = specialistsDiploma.IdUser, DiplomaPath = specialistsDiploma.DiplomaPath, Verified = (bool) specialistsDiploma.Verified };
        }

        private List<SpecialistsDiplomaDAO> ConvertSpecialistsDiplomasToDAOs(List<SpecialitsDiploma> specialistsDiplomas)
        {
            List<SpecialistsDiplomaDAO> specialistsDiplomasDAO = new List<SpecialistsDiplomaDAO>();
            foreach (SpecialitsDiploma sd in specialistsDiplomas)
            {
                specialistsDiplomasDAO.Add(ConvertSpecialistsDiplomaToDAO(sd));
            }
            return specialistsDiplomasDAO;
        }

        public List<SpecialistsDiplomaDAO> GetAll()
        {
            List<SpecialitsDiploma> sp = new NewPassiveHouseEntities().SpecialitsDiplomas.ToList();
            return ConvertSpecialistsDiplomasToDAOs(sp);
        }

        public List<SpecialistsDiplomaDAO> GetDiplomasOfSpecificSpecialist(String userId)
        {
            List<SpecialitsDiploma> sp = new List<SpecialitsDiploma>();
            using (var context = new NewPassiveHouseEntities())
            {
                sp = context.SpecialitsDiplomas.Where(m => m.IdUser.Equals(userId)).ToList();
            }
            return ConvertSpecialistsDiplomasToDAOs(sp);
        }

        public void AddDiploma(SpecialistsDiplomaDAO spDAO)
        {
            SpecialitsDiploma sp = ConvertDAOToSpecialitsDiploma(spDAO);
            using (var context = new NewPassiveHouseEntities())
            {
                context.SpecialitsDiplomas.Add(sp);
                context.SaveChanges();           
            }
        }

        public bool MarkDiplomaAsVerified(int id)
        {
            using (var context = new NewPassiveHouseEntities())
            {
                SpecialitsDiploma foundSpecialistsDiploma = null;
                if (context.SpecialitsDiplomas.Any(x => x.Id == id))
                {
                    foundSpecialistsDiploma = context.SpecialitsDiplomas.First(x => x.Id == id);
                    if (foundSpecialistsDiploma.Verified == true)
                    {
                        return false;
                    }
                } else
                {
                    return false;
                }

                foundSpecialistsDiploma.Verified = true;
                context.SaveChanges();              
            }
            return true;
        }
    }
}
