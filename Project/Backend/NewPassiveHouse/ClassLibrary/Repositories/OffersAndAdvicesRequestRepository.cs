﻿using ClassLibrary.DAOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repositories
{
    public class OffersAndAdvicesRequestRepository
    {
        private OffersAndAdvicesRequestDAO ConvertToDAO(OffersAndAdvicesRequest request)
        {
            return new OffersAndAdvicesRequestDAO { Id = request.Id, UserId = request.UserId, PassiveHouseId = request.PassiveHouseId, Description = request.Description };
        }

        private OffersAndAdvicesRequest ConvertFromDAO(OffersAndAdvicesRequestDAO request)
        {
            return new OffersAndAdvicesRequest { Id = request.Id, UserId = request.UserId, PassiveHouseId = request.PassiveHouseId, Description = request.Description };
        }

        private List<OffersAndAdvicesRequestDAO> ConvertToDAOs(List<OffersAndAdvicesRequest> requests)
        {
            List<OffersAndAdvicesRequestDAO> requestsDAO = new List<OffersAndAdvicesRequestDAO>();
            foreach (OffersAndAdvicesRequest request in requests)
            {
                requestsDAO.Add(ConvertToDAO(request));
            }
            return requestsDAO;
        }

        public void AddOffersAndAdvicesRequest(OffersAndAdvicesRequestDAO requestDAO)
        {
            OffersAndAdvicesRequest request = ConvertFromDAO(requestDAO);
            using (var context = new NewPassiveHouseEntities())
            {
                context.OffersAndAdvicesRequests.Add(request);
                context.SaveChanges();
            }
        }

        public List<OffersAndAdvicesRequestDAO> GetRequestsOfSpecificUser(string userId)
        {
            List<OffersAndAdvicesRequest> requests = new List<OffersAndAdvicesRequest>();
            using (var context = new NewPassiveHouseEntities())
            {
                requests = context.OffersAndAdvicesRequests.Where(m => m.UserId.Equals(userId)).ToList();
            }
            return ConvertToDAOs(requests);
        }

        public List<OffersAndAdvicesRequestDAO> GetRequestsForSpecificPassiveHouse(int passiveHouseId)
        {
            List<OffersAndAdvicesRequest> requests = new List<OffersAndAdvicesRequest>();
            using (var context = new NewPassiveHouseEntities())
            {
                requests = context.OffersAndAdvicesRequests.Where(m => m.PassiveHouseId == passiveHouseId).ToList();
            }
            return ConvertToDAOs(requests);
        }

        private bool RequestHasResponse(int IdRequest)
        {
            return new NewPassiveHouseEntities().OffersAndAdvicesResponses.Any(m => m.RequestId == IdRequest);
        }

        public List<OffersAndAdvicesRequestDAO> GetUnansweredRequests()
        {
            List<OffersAndAdvicesRequest> requests = new List<OffersAndAdvicesRequest>();
            using (var context = new NewPassiveHouseEntities())
            {
                foreach (OffersAndAdvicesRequest request in context.OffersAndAdvicesRequests)
                {
                    if (RequestHasResponse(request.Id) == false)
                    {
                        requests.Add(request);
                    }
                }
            }
            return ConvertToDAOs(requests);
        }

        public List<OffersAndAdvicesRequestDAO> GetAllRequests()
        {
            List<OffersAndAdvicesRequest> requests = new NewPassiveHouseEntities().OffersAndAdvicesRequests.ToList();
            return ConvertToDAOs(requests);
        }
    }
}
