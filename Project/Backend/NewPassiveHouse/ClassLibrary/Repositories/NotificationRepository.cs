﻿using ClassLibrary.DAOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repositories
{
    public class NotificationRepository
    {
        public NotificationDAO ConvertToDAO(Notification notification)
        {
            return new NotificationDAO { Id = notification.Id, Message = notification.Message, Type = notification.Type };
        }

        public NotificationDAO[] ConvertToDAOs(Notification[] ns)
        {
            NotificationDAO[] nDAOs = new NotificationDAO[ns.Length];
            for (int i = 0; i < ns.Length; i++)
            {
                nDAOs[i] = ConvertToDAO(ns[i]);
            }
            return nDAOs;
        }

        public NotificationDAO[] GetAllNotifications()
        {
            return ConvertToDAOs(new NewPassiveHouseEntities().Notifications.ToArray());
        }
    }
}
