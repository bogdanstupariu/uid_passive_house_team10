﻿using ClassLibrary.DAOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repositories
{
    public class UserRepository
    {
        private PassiveHouseRepository phRep;

        public UserRepository()
        {
            phRep = new PassiveHouseRepository();
        }

        public AspNetUser GetUserWithSpecificId(string Id)
        {
            using (var context = new NewPassiveHouseEntities())
            {
                return context.AspNetUsers.FirstOrDefault(x => x.Id == Id);
            }
        }

        public PassiveHouseDAO GetUserPassiveHouse(string userId)
        {
            using (var context = new NewPassiveHouseEntities())
            {
                PassiveHous ph = context.PassiveHouses.FirstOrDefault(x => x.UserID.Equals(userId));
                if (ph != null)
                {
                    return phRep.ConvertPassiveHouseToDAO(ph);
                }
                return null;
            }
        }

        public AspNetUser[] GetAllUsers()
        {
            using (var context = new NewPassiveHouseEntities())
            {
                return context.AspNetUsers.ToArray();
            }
        }

        public string GetRoleIdForSpecificRoleName(string roleName)
        {
            using (var context = new NewPassiveHouseEntities())
            {
                return context.AspNetRoles.FirstOrDefault(x => x.Name.Equals(roleName)).Id;
            }
        }

    }
}
