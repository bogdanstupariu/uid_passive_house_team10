﻿using ClassLibrary.DAOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repositories
{
    public class PassiveHouseRepository
    {
        private PassiveHous ConvertDAOToPassiveHouse(PassiveHouseDAO phDAO)
        {
            return new PassiveHous { Id = phDAO.Id, Area = phDAO.Area, MonthlyConsumption = phDAO.MonthlyConsumption, NoOfRooms = phDAO.NoOfRooms, UserID = phDAO.UserID,
                Description = phDAO.Description, Address = phDAO.Address, Picture = phDAO.Picture, Budget = phDAO.Budget };
        }

        public PassiveHouseDAO ConvertPassiveHouseToDAO(PassiveHous ph)
        {
            return new PassiveHouseDAO { Id = ph.Id, UserID = ph.UserID, Area = ph.Area, MonthlyConsumption = ph.MonthlyConsumption, NoOfRooms = ph.NoOfRooms,
                Description = ph.Description, Address = ph.Address, Picture = ph.Picture, Budget = ph.Budget };
        }

        public List<PassiveHouseDAO> ConvertToDAOs(List<PassiveHous> phs)
        {
            List<PassiveHouseDAO> phDAOs = new List<PassiveHouseDAO>();
            foreach (PassiveHous ph in phs)
            {
                phDAOs.Add(ConvertPassiveHouseToDAO(ph));
            }
            return phDAOs;
        }

        public List<PassiveHouseDAO> GetAllHouses()
        {
            List<PassiveHous> phs = new NewPassiveHouseEntities().PassiveHouses.ToList();
            return ConvertToDAOs(phs);
        }

        public void AddPassiveHouse(PassiveHouseDAO phDAO)
        {
            PassiveHous ph = ConvertDAOToPassiveHouse(phDAO);
            using (var context = new NewPassiveHouseEntities())
            {
                context.PassiveHouses.Add(ph);
                context.SaveChanges();
            }
        }

        public PassiveHouseDAO GetPassiveHouseWithSpecificId(int id)
        {
            using (var context = new NewPassiveHouseEntities())
            {
                PassiveHous ph = context.PassiveHouses.FirstOrDefault(x => x.Id == id);
                if (ph == null)
                {
                    return null;
                }
                return ConvertPassiveHouseToDAO(ph);
            }
        }
    }
}
