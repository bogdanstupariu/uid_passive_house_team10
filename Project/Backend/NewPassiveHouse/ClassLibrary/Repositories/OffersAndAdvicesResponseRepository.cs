﻿using ClassLibrary.DAOs;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.Repositories
{
    public class OffersAndAdvicesResponseRepository
    {
        private OffersAndAdvicesResponseDAO ConvertToDAO(OffersAndAdvicesResponse response)
        {
            return new OffersAndAdvicesResponseDAO { Id = response.Id, UserId = response.UserId, RequestId = response.RequestId, Description = response.Description };
        }

        private OffersAndAdvicesResponse ConvertFromDAO(OffersAndAdvicesResponseDAO response)
        {
            return new OffersAndAdvicesResponse { Id = response.Id, UserId = response.UserId, RequestId = response.RequestId, Description = response.Description };
        }

        private List<OffersAndAdvicesResponseDAO> ConvertToDAOs(List<OffersAndAdvicesResponse> responses)
        {
            List<OffersAndAdvicesResponseDAO> responseDAOs = new List<OffersAndAdvicesResponseDAO>();
            foreach (OffersAndAdvicesResponse response in responses)
            {
                responseDAOs.Add(ConvertToDAO(response));
            }
            return responseDAOs;
        }

        public List<OffersAndAdvicesResponseDAO> GetResponsesOfSpecificUser(object p)
        {
            throw new NotImplementedException();
        }

        public void AddOffersAndAdvicesResponse(OffersAndAdvicesResponseDAO responseDAO)
        {
            OffersAndAdvicesResponse response = ConvertFromDAO(responseDAO);
            using (var context = new NewPassiveHouseEntities())
            {
                try
                {
                    context.OffersAndAdvicesResponses.Add(response);
                    context.SaveChanges();
                } catch(Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        public List<OffersAndAdvicesResponseDAO> GetResponsesOfSpecificUser(string userId)
        {
            List<OffersAndAdvicesResponse> responses = new List<OffersAndAdvicesResponse>();
            using (var context = new NewPassiveHouseEntities())
            {
                responses = context.OffersAndAdvicesResponses.Where(m => m.UserId.Equals(userId)).ToList();
            }
            return ConvertToDAOs(responses);
        }

        public List<OffersAndAdvicesResponseDAO> GetResponsesForSpecificRequest(int requestId)
        {
            List<OffersAndAdvicesResponse> responses = new List<OffersAndAdvicesResponse>();
            using (var context = new NewPassiveHouseEntities())
            {
                responses = context.OffersAndAdvicesResponses.Where(m => m.RequestId == requestId).ToList();
            }
            return ConvertToDAOs(responses);
        }
    }
}
