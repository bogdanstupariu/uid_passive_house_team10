﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAOs
{
    public class OffersAndAdvicesRequestDAO
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int PassiveHouseId { get; set; }
        public string Description { get; set; }
    }
}
