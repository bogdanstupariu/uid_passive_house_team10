﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAOs
{
    public class EnergyDAO
    {
        public int Id { get; set; }
        public string Type { get; set; }
        public decimal Value { get; set; }
    }
}
