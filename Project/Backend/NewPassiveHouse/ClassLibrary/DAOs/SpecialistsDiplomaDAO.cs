﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary.DAOs
{
    public class SpecialistsDiplomaDAO
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public string DiplomaPath { get; set; }
        public bool Verified { get; set; }
    }
}
