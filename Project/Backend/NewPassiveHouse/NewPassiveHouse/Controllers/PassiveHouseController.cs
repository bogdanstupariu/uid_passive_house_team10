﻿using ClassLibrary.DAOs;
using ClassLibrary.Repositories;
using Microsoft.AspNet.Identity;
using NewPassiveHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewPassiveHouse.Controllers
{
    [Authorize]
    public class PassiveHouseController : ApiController
    {
        private PassiveHouseRepository phRepository;

        public PassiveHouseController()
        {
            phRepository = new PassiveHouseRepository();
        }

        //GET: api/PassiveHouse/GetAllHouses
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetAllHouses()
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                List<PassiveHouseDAO> phDAOList = phRepository.GetAllHouses();
                return Json(phDAOList);
            }
            return Json("You need to be logged in as an Administrator or as a Specialist to access this resource.");
        }

        // POST: api/PassiveHouse/AddPassiveHouse
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult AddPassiveHouse(PassiveHouseModel ph)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                string currentUserId = User.Identity.GetUserId();
                phRepository.AddPassiveHouse(new PassiveHouseDAO {
                    UserID = currentUserId, NoOfRooms = ph.NoOfRooms, Area = ph.Area, MonthlyConsumption = ph.MonthlyConsumption,
                    Address = ph.Address, Picture = ph.Picture, Description = ph.Description, Budget = ph.Budget
                });
                return Json("Passive house was successfully added.");
            }
            return Json("You need to be logged in as an CommonUser to access this resource.");
        }

        // GET: api/PassiveHouse/GetNoOfPanels
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetNoOfPanels(int passiveHouseId)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                PassiveHouseDAO ph = phRepository.GetPassiveHouseWithSpecificId(passiveHouseId);
                if (ph != null)
                {
                    int noOfPanels = (int)ph.MonthlyConsumption / 30 / 24 / 250;
                    return Json(noOfPanels);
                }
                return Json("There is no passive house having this id.");
            }
            return Json("You need to be logged in as an CommonUser to access this resource.");
        }

        // GET: api/PassiveHouse/GetHourlyEnergyBasedOnLocation
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetHourlyEnergyBasedOnLocation(int passiveHouseId, decimal d)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                PassiveHouseDAO ph = phRepository.GetPassiveHouseWithSpecificId(passiveHouseId);
                if (ph != null)
                {
                    decimal energy = (ph.MonthlyConsumption / 30 / 24) * 1000 / d ;
                    return Json(energy);
                }
                return Json("There is no passive house having this id.");
            }
            return Json("You need to be logged in as an CommonUser to access this resource.");
        }

        // GET: api/PassiveHouse/GetCostsPerMonth
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetCosts(int passiveHouseId, int noOfSolarPanels)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                PassiveHouseDAO ph = phRepository.GetPassiveHouseWithSpecificId(passiveHouseId);
                if (ph != null)
                {
                    decimal cost = noOfSolarPanels * 250 * (decimal)3.25;
                    return Json(cost);
                }
                return Json("There is no passive house having this id.");
            }
            return Json("You need to be logged in as an CommonUser to access this resource.");
        }
    }
}
