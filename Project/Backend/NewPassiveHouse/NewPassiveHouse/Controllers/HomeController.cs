﻿using ClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewPassiveHouse.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        [Authorize]
        public ActionResult GetAllHouses()
        {
            List<PassiveHous> houses = new NewPassiveHouseEntities().PassiveHouses.ToList();
            return Json(houses, JsonRequestBehavior.AllowGet);
        }
    }
}
