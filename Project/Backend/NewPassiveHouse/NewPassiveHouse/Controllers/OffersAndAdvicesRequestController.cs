﻿using ClassLibrary.DAOs;
using ClassLibrary.Repositories;
using Microsoft.AspNet.Identity;
using NewPassiveHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewPassiveHouse.Controllers
{
    [Authorize]
    public class OffersAndAdvicesRequestController : ApiController
    {
        private OffersAndAdvicesRequestRepository oarRepository;

        public OffersAndAdvicesRequestController()
        {
            oarRepository = new OffersAndAdvicesRequestRepository();
        }

        // GET: api/OffersAndAdvicesRequest/GetAllRequests
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetAllRequests()
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator"))
            {
                List<OffersAndAdvicesRequestDAO> oarDAO = oarRepository.GetAllRequests();
                return Json(oarDAO);
            }
            return Json("You need to be logged in as a Specialist to access this resource.");
        }

        // GET: api/OffersAndAdvicesRequest/GetUnansweredRequests
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetUnansweredRequests()
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator"))
            {
                List<OffersAndAdvicesRequestDAO> oarDAO = oarRepository.GetUnansweredRequests();
                return Json(oarDAO);
            }
            return Json("You need to be logged in as a Specialist to access this resource.");
        }

        // GET: api/OffersAndAdvicesRequest/GetRequestsForSpecificPassiveHouse
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetRequestsForSpecificPassiveHouse(int passiveHouseId)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                List<OffersAndAdvicesRequestDAO> oarDAO = oarRepository.GetRequestsForSpecificPassiveHouse(passiveHouseId);
                return Json(oarDAO);
            }
            return Json("You need to be logged in as a Specialist to access this resource.");
        }

        // GET: api/OffersAndAdvicesRequest/GetRequestsOfSpecificUser
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetRequestsOfSpecificUser(string userId)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                List<OffersAndAdvicesRequestDAO> oarDAO = oarRepository.GetRequestsOfSpecificUser(userId);
                return Json(oarDAO);
            }
            return Json("You need to be logged in as a Specialist to access this resource.");
        }

        // POST: api/OffersAndAdvicesRequest/AddOffersAndAdvicesRequest
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult AddOffersAndAdvicesRequest(OffersAndAdvicesRequestModel model)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                var requestDAO = new OffersAndAdvicesRequestDAO { PassiveHouseId = model.PassiveHouseId, UserId = User.Identity.GetUserId(), Description = model.Description };
                oarRepository.AddOffersAndAdvicesRequest(requestDAO);
                return Json("Request successfully added.");
            }
            return Json("You need to be logged in as a CommonUser to access this resource.");
        }
    }
}
