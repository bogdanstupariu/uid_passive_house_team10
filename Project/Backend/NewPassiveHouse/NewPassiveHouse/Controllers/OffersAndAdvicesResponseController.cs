﻿using ClassLibrary.DAOs;
using ClassLibrary.Repositories;
using Microsoft.AspNet.Identity;
using NewPassiveHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewPassiveHouse.Controllers
{
    public class OffersAndAdvicesResponseController : ApiController
    {
        private OffersAndAdvicesResponseRepository oarRepository;
        private SpecialistsDiplomaRepository sdRepository;

        public OffersAndAdvicesResponseController()
        {
            oarRepository = new OffersAndAdvicesResponseRepository();
            sdRepository = new SpecialistsDiplomaRepository();
        }

        // GET: api/OffersAndAdvicesResponse/GetResponsesOfSpecificUser
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetResponsesOfSpecificUser()
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator"))
            {
                List<OffersAndAdvicesResponseDAO> oarDAO = oarRepository.GetResponsesOfSpecificUser(User.Identity.GetUserId());
                return Json(oarDAO);
            }
            return Json("You need to be logged in as a Specialist to access this resource.");
        }

        // GET: api/OffersAndAdvicesResponse/GetResponsesForSpecificRequest
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetResponsesForSpecificRequest(int requestId)
        {
            if (User.IsInRole("CommonUser") || User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                List<OffersAndAdvicesResponseDAO> oarDAO = oarRepository.GetResponsesForSpecificRequest(requestId);
                return Json(oarDAO);
            }
            return Json("You need to be logged in as a CommonUser to access this resource.");
        }

        // POST: api/OffersAndAdvicesResponse/AddOffersAndAdvicesResponse
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult AddOffersAndAdvicesResponse(OffersAndAdvicesResponseModel model)
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator"))
            {
                var currentUserID = User.Identity.GetUserId();
                List<SpecialistsDiplomaDAO> diplomas = sdRepository.GetDiplomasOfSpecificSpecialist(currentUserID);
                if (diplomas.Count() > 0 && diplomas.Any(m => m.Verified == true))
                {
                    var requestDAO = new OffersAndAdvicesResponseDAO { RequestId = model.RequestId, UserId = currentUserID, Description = model.Description };
                    oarRepository.AddOffersAndAdvicesResponse(requestDAO);
                    return Json("Response successfully added.");
                }
                return Json("This specialist has no verified diplomas. He cannot add a response.");
            }
            return Json("You need to be logged in as a Specialist to access this resource.");
        }
    }
}
