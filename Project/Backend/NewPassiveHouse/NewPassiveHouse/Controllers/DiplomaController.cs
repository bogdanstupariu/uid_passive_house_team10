﻿using ClassLibrary.DAOs;
using ClassLibrary.Repositories;
using NewPassiveHouse.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using ClassLibrary;
using Microsoft.AspNet.Identity.EntityFramework;

namespace NewPassiveHouse.Controllers
{

    [Authorize]
    public class DiplomaController : ApiController
    {
        SpecialistsDiplomaRepository sdRepository;
        UserRepository userRepository;

        public DiplomaController()
        {
            sdRepository = new SpecialistsDiplomaRepository();
            userRepository = new UserRepository();
        }

        private SpecialistsDiplomaDAO[] ConvertListToArray(List<SpecialistsDiplomaDAO> sdDAOList)
        {
            SpecialistsDiplomaDAO[] sdDAOArray = new SpecialistsDiplomaDAO[sdDAOList.Count];
            int i = 0;
            foreach (SpecialistsDiplomaDAO sdDAO in sdDAOList)
            {
                sdDAOArray[i] = sdDAO;
            }
            return sdDAOArray;
        }


        // GET: api/Diploma/GetAllDiplomas
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetAllDiplomas()
        {
            if (User.IsInRole("Administrator"))
            {
                List<SpecialistsDiplomaDAO> sdDAO = sdRepository.GetAll();
                //List<SpecialistsDiplomaDAO> sdDAOList = sdRepository.GetAll();
                //SpecialistsDiplomaDAO[] sdDAOArray = ConvertListToArray(sdDAOList);
                //return Json(sdDAOArray);

                return Json(sdDAO);
            }
            return Json("You need to be logged in as an Administrator to access this resource.");
        }

        // GET: api/Diploma/GetDiplomasOfSpecificSpecialist
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetDiplomasOfSpecificSpecialist(string Id)
        {
            if (User.IsInRole("Administrator") || User.IsInRole("Specialist"))
            {
                List<SpecialistsDiplomaDAO> sdDAOs= sdRepository.GetDiplomasOfSpecificSpecialist(Id);
                return Json(sdDAOs);
            }
            return Json("You need to be logged in as an Administrator to access this resource.");
        }

        // POST: api/Diploma/AddDiploma
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult AddDiploma(string diplomaPath)
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator"))
            {
                string currentUserId = User.Identity.GetUserId();
                sdRepository.AddDiploma(new SpecialistsDiplomaDAO { UserId = currentUserId, DiplomaPath = diplomaPath, Verified = false });
                return Json("Diploma was successfully added.");
            }
            return Json("You need to be logged in as an Specialist to access this resource.");
        }

        // POST: api/Diploma/MarkDiplomaAsVerified
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpPost]
        public IHttpActionResult MarkDiplomaAsVerified(int id)
        {
            if (User.IsInRole("Administrator"))
            {
                var ok = sdRepository.MarkDiplomaAsVerified(id);
                if (ok == true)
                {
                    return Json("The diploma was marked as verified.");
                }
                else
                {
                    return Json("The diploma does not exist or was already marked as verified.");
                }
            }
            return Json("You need to be logged in as an Administrator to access this resource.");
        }

    }
}
