﻿using ClassLibrary;
using ClassLibrary.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using NewPassiveHouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace NewPassiveHouse.Controllers
{

    [Authorize]
    public class UserController : ApiController
    {
        private UserRepository userRep;

        public UserController()
        {
            userRep = new UserRepository();
        }

        // GET: api/User/GetAllUsersWithSpecificRole
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public async Task<IHttpActionResult> GetAllUsersWithSpecificRole(string roleName)
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator") || User.IsInRole("CommonUser"))
            {
                AspNetUser[] users = userRep.GetAllUsers();
                ApplicationUser[] usersWithSpecificRole = new ApplicationUser[users.Length];


                ApplicationDbContext context = new ApplicationDbContext();
                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

                var roleId = userRep.GetRoleIdForSpecificRoleName(roleName);


                int i = 0;
                foreach (AspNetUser user in users)

                {
                    var userFound = await userManager.FindByNameAsync(user.UserName);
                    var roles = userFound.Roles;
                    foreach (IdentityUserRole role in roles)
                    {
                        if (role.RoleId.Equals(roleId))
                        {
                            usersWithSpecificRole[i] = userFound;
                            i++;
                        }

                    }
                }
                return Json(usersWithSpecificRole);
            }
            return Json("You need to be logged in to access this resource.");

        }
    }
}
