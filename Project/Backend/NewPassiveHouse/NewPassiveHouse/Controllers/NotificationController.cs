﻿using ClassLibrary.DAOs;
using ClassLibrary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewPassiveHouse.Controllers
{
    [Authorize]
    public class NotificationController : ApiController
    {
        private NotificationRepository nRep;

        public NotificationController()
        {
            nRep = new NotificationRepository();
        }

        //GET: api/Notification/GetAllNotifications
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetAllNotifications()
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator") || User.IsInRole("CommonUser"))
            {
                NotificationDAO[] nDAOList = nRep.GetAllNotifications();
                return Json(nDAOList);
            }
            return Json("You need to be logged in to access this resource.");

        }
    }
}
