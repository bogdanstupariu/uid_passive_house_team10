﻿using ClassLibrary.DAOs;
using ClassLibrary.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace NewPassiveHouse.Controllers
{
    [Authorize]
    public class EnergyController : ApiController
    {
        private EnergyRepository erRep;

        public EnergyController()
        {
            erRep = new EnergyRepository();
        }

        //GET: api/Energy/GetAll
        [System.Web.Http.AcceptVerbs("GET", "POST")]
        [System.Web.Http.HttpGet]
        public IHttpActionResult GetAll()
        {
            if (User.IsInRole("Specialist") || User.IsInRole("Administrator") || User.IsInRole("CommonUser"))
            {
                EnergyDAO[] eDAOList = erRep.GetAll();
                return Json(eDAOList);
            }
            return Json("You need to be logged in to access this resource.");

        }
    }
}
