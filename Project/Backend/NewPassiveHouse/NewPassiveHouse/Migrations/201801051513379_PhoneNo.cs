namespace NewPassiveHouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhoneNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "PhoneNo", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "PhoneNo");
        }
    }
}
