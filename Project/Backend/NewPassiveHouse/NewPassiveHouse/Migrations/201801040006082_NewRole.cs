namespace NewPassiveHouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewRole : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AspNetUsers", "Role_Id", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUsers", new[] { "Role_Id" });
            AddColumn("dbo.AspNetUsers", "Role", c => c.String());
            DropColumn("dbo.AspNetUsers", "Role_Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Role_Id", c => c.String(maxLength: 128));
            DropColumn("dbo.AspNetUsers", "Role");
            CreateIndex("dbo.AspNetUsers", "Role_Id");
            AddForeignKey("dbo.AspNetUsers", "Role_Id", "dbo.AspNetRoles", "Id");
        }
    }
}
