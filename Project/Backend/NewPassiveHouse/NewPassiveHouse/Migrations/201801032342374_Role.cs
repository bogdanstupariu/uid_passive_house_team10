namespace NewPassiveHouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Role : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.AspNetUsers", name: "IdRole_Id", newName: "Role_Id");
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_IdRole_Id", newName: "IX_Role_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_Role_Id", newName: "IX_IdRole_Id");
            RenameColumn(table: "dbo.AspNetUsers", name: "Role_Id", newName: "IdRole_Id");
        }
    }
}
