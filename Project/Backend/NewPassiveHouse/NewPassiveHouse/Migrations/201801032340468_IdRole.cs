namespace NewPassiveHouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class IdRole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "IdRole_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.AspNetUsers", "IdRole_Id");
            AddForeignKey("dbo.AspNetUsers", "IdRole_Id", "dbo.AspNetRoles", "Id");
            DropColumn("dbo.AspNetUsers", "Role");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "Role", c => c.String());
            DropForeignKey("dbo.AspNetUsers", "IdRole_Id", "dbo.AspNetRoles");
            DropIndex("dbo.AspNetUsers", new[] { "IdRole_Id" });
            DropColumn("dbo.AspNetUsers", "IdRole_Id");
        }
    }
}
