namespace NewPassiveHouse.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PhoneNoWithout : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.AspNetUsers", "PhoneNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "PhoneNo", c => c.String());
        }
    }
}
