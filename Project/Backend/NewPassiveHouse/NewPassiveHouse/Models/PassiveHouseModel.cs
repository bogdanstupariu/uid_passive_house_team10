﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewPassiveHouse.Models
{
    public class PassiveHouseModel
    {
        public int NoOfRooms { get; set; }
        public decimal Area { get; set; }
        public decimal MonthlyConsumption { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public string Picture { get; set; }
        public string Budget { get; set; }

    }
}