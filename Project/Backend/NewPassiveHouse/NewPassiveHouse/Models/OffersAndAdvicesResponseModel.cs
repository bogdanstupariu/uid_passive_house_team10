﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewPassiveHouse.Models
{
    public class OffersAndAdvicesResponseModel
    {
        public int RequestId { get; set; }
        public string Description { get; set; }
    }
}