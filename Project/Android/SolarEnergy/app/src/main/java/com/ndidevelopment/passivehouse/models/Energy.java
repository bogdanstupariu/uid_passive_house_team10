package com.ndidevelopment.passivehouse.models;

/**
 * Created by Andi on 1/16/2018.
 */

public class Energy {
    private int Id;
    private String Type;
    private Float value;

    public int getId() {
        return Id;
    }

    public Energy setId(int id) {
        Id = id;
        return this;
    }

    public String getType() {
        return Type;
    }

    public Energy setType(String type) {
        Type = type;
        return this;
    }

    public Float getValue() {
        return value;
    }

    public Energy setValue(Float value) {
        this.value = value;
        return this;
    }
}
