package com.ndidevelopment.passivehouse.rest;

import com.ndidevelopment.passivehouse.models.Energy;
import com.ndidevelopment.passivehouse.models.LoginRequest;
import com.ndidevelopment.passivehouse.models.LoginResponse;
import com.ndidevelopment.passivehouse.models.Notification;
import com.ndidevelopment.passivehouse.models.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Bia on 1/16/2018.
 */

public interface Api {

    @GET("api/Notification/GetAllNotifications")
    Call<List<Notification>> getAllNotifications(@Header("Authorization") String token);

    @GET("api/Energy/GetAll")
    Call<List<Energy>> getAllEnergyData(@Header("Authorization") String token);

    @GET("api/User/GetAllUsersWithSpecificRole")
    Call<List<User>> getUsersInfo(@Query("roleName") String roleName,
                                  @Header("Authorization") String token);

    @POST("token")
    @Headers("Content-Type: text/html")
    Call<LoginResponse> login(@Body LoginRequest loginRequest);


}
