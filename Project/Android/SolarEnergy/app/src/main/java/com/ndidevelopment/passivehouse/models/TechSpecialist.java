package com.ndidevelopment.passivehouse.models;

/**
 * Created by Andi on 1/6/2018.
 */

public class TechSpecialist {

    private String name;
    private String description;
    private String phoneNr;

    public TechSpecialist(String name, String description, String phoneNr) {
        this.name = name;
        this.description = description;
        this.phoneNr = phoneNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhoneNr() {
        return phoneNr;
    }

    public void setPhoneNr(String phoneNr) {
        this.phoneNr = phoneNr;
    }
}
