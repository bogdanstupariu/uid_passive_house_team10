package com.ndidevelopment.passivehouse.ui;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.ndidevelopment.passivehouse.R;
import com.ndidevelopment.passivehouse.models.LoginRequest;
import com.ndidevelopment.passivehouse.models.LoginResponse;
import com.ndidevelopment.passivehouse.rest.RetrofitInstance;
import com.tbruyelle.rxpermissions2.RxPermissions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    private static final String USERNAME = "mary.brown@gmail.com";
    private static final String PASSWORD = "Parola1!";

    @BindView(R.id.login_edittext_username)
    EditText usernameEditText;

    @BindView(R.id.login_edittext_password)
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.login_button_login)
    protected void loginClicked() {
        startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
        /*LoginRequest loginRequest = new LoginRequest()
                .setUsername(usernameEditText.getText().toString())
                .setPassword(passwordEditText.getText().toString())
                .setGrant_type("password");
        RetrofitInstance.getInstance().getApi()
                .login(loginRequest)
                .enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response.isSuccessful()) {
                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit()
                                    .putString("token", response.body().getAccess_token())
                                    .putString("username", response.body().getUsername())
                                    .apply();
                            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
                        } else {
                            Toast.makeText(LoginActivity.this, response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        Toast.makeText(LoginActivity.this, "Failure", Toast.LENGTH_SHORT).show();
                    }
                });*/

    }
}
