package com.ndidevelopment.passivehouse.ui.techsupport;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ndidevelopment.passivehouse.R;
import com.ndidevelopment.passivehouse.models.TechSpecialist;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Bia on 1/6/2018.
 */

public class TechSupportAdapter extends RecyclerView.Adapter<TechSupportAdapter.ViewHolder> {

    private List<TechSpecialist> techSpecialistList;
    private TechSupportCallback callback;

    public TechSupportAdapter(List<TechSpecialist> specialistList, TechSupportCallback callback) {
        this.techSpecialistList = specialistList;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.techspecialist_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.specialistNameTextView.setText(techSpecialistList.get(position).getName());
        holder.specialistDescritpionTextView.setText(techSpecialistList.get(position).getDescription());
    }

    @Override
    public int getItemCount() {
        return techSpecialistList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.specialistrow_textview_name)
        TextView specialistNameTextView;

        @BindView(R.id.specialistrow_textview_description)
        TextView specialistDescritpionTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(view -> callback.callClicked(getAdapterPosition()));
        }
    }
}
