package com.ndidevelopment.passivehouse.models;

/**
 * Created by Andi on 1/16/2018.
 */

public class Role {

    private String UserId;
    private String RoleId;

    public String getUserId() {
        return UserId;
    }

    public Role setUserId(String userId) {
        UserId = userId;
        return this;
    }

    public String getRoleId() {
        return RoleId;
    }

    public Role setRoleId(String roleId) {
        RoleId = roleId;
        return this;
    }
}
