package com.ndidevelopment.passivehouse.models;

/**
 * Created by Andi on 1/16/2018.
 */

public class LoginRequest {
    private String username;
    private String password;
    private String grant_type;

    public String getUsername() {
        return username;
    }

    public LoginRequest setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public LoginRequest setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getGrant_type() {
        return grant_type;
    }

    public LoginRequest setGrant_type(String grant_type) {
        this.grant_type = grant_type;
        return this;
    }
}
