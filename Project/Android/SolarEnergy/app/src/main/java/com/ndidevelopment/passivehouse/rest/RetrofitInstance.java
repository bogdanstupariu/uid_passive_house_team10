package com.ndidevelopment.passivehouse.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Bia on 1/16/2018.
 */

public class RetrofitInstance {

    private static RetrofitInstance instance;
    private Api api;

    private RetrofitInstance() {
        init();
    }

    public static RetrofitInstance getInstance() {
        if (instance == null) {
            instance = new RetrofitInstance();
        }
        return instance;
    }

    private void init() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://192.168.43.123:2766/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        api = retrofit.create(Api.class);
    }

    public Api getApi() {
        return instance.api;
    }
}
