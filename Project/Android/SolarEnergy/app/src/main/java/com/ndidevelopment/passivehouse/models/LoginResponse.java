package com.ndidevelopment.passivehouse.models;

/**
 * Created by Andi on 1/16/2018.
 */

public class LoginResponse {
    private String access_token;
    private String username;

    public String getAccess_token() {
        return access_token;
    }

    public String getUsername() {
        return username;
    }
}
