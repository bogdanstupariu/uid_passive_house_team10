package com.ndidevelopment.passivehouse.models;

import java.util.List;

/**
 * Created by Andi on 1/16/2018.
 */

public class User {

    private String FirstName;
    private String LastName;
    private String Email;
    private String PhoneNumber;
    private String UserName;
    private List<Role> Roles;

    public String getFirstName() {
        return FirstName;
    }

    public User setFirstName(String firstName) {
        FirstName = firstName;
        return this;
    }

    public String getLastName() {
        return LastName;
    }

    public User setLastName(String lastName) {
        LastName = lastName;
        return this;
    }

    public String getEmail() {
        return Email;
    }

    public User setEmail(String email) {
        Email = email;
        return this;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public User setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
        return this;
    }

    public String getUserName() {
        return UserName;
    }

    public User setUserName(String userName) {
        UserName = userName;
        return this;
    }

    public List<Role> getRoles() {
        return Roles;
    }

    public User setRoles(List<Role> roles) {
        Roles = roles;
        return this;
    }
}
