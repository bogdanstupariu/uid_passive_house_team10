package com.ndidevelopment.passivehouse.models;

/**
 * Created by Andi on 1/15/2018.
 */

public class Notification {

    private int Id;
    private String Message;
    private String Type;

    public Notification(int id, String Message, String type) {
        this.Id = id;
        this.Message = Message;
        this.Type = type;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String text) {
        this.Message = text;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        this.Type = type;
    }
}
