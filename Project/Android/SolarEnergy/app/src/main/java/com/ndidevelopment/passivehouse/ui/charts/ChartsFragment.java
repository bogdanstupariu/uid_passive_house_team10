package com.ndidevelopment.passivehouse.ui.charts;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.ndidevelopment.passivehouse.R;
import com.ndidevelopment.passivehouse.models.Energy;
import com.ndidevelopment.passivehouse.rest.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Andi on 1/7/2018.
 */

public class ChartsFragment extends Fragment {

    @BindView(R.id.fr_charts_button_daily)
    Button dailyButton;

    @BindView(R.id.fr_charts_button_weekly)
    Button weeklyButton;

    @BindView(R.id.fr_charts_button_monthly)
    Button monthlyButton;

    @BindView(R.id.fr_charts_chart_data)
    BarChart dataLineChart;

    private List<Energy> dailyData;
    private List<Energy> weeklyData;
    private List<Energy> monthlyData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_charts, container, false);
        ButterKnife.bind(this, view);
        selectDaily();

        dailyData = new ArrayList<>();
        weeklyData = new ArrayList<>();
        monthlyData = new ArrayList<>();

        /*RetrofitInstance.getInstance().getApi()
                .getAllEnergyData(PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""))
                .enqueue(new Callback<List<Energy>>() {
                    @Override
                    public void onResponse(Call<List<Energy>> call, Response<List<Energy>> response) {
                        if (response.isSuccessful()) {
                            dailyData.clear();
                            weeklyData.clear();
                            monthlyData.clear();
                            response.body()
                                    .forEach(d -> {
                                        if (d.getType().equals("Daily")) {
                                            dailyData.add(d);
                                        } else if (d.getType().equals("Weekly")) {
                                            weeklyData.add(d);
                                        } else if (d.getType().equals("Monthly")) {
                                            monthlyData.add(d);
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Energy>> call, Throwable t) {

                    }
                });*/

        return view;
    }

    @OnClick({R.id.fr_charts_button_daily, R.id.fr_charts_button_weekly, R.id.fr_charts_button_monthly})
    void chartModeClicked(View v) {
        dailyButton.setBackgroundResource(R.color.colorPrimary);
        weeklyButton.setBackgroundResource(R.color.colorPrimary);
        monthlyButton.setBackgroundResource(R.color.colorPrimary);

        switch (v.getId()) {
            case R.id.fr_charts_button_daily:
                selectDaily();
                break;
            case R.id.fr_charts_button_weekly:
                selectWeekly();
                break;
            case R.id.fr_charts_button_monthly:
                selectMonthly();
                break;
        }

    }

    private void selectMonthly() {
        monthlyButton.setBackgroundResource(R.color.colorPrimaryDark);

        Random random = new Random();

        List<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            entries.add(new BarEntry(i, 500 + random.nextInt(200)));
        }

        /*for (int i = 0; i < monthlyData.size(); i++) {
            entries.add(new BarEntry(i, monthlyData.get(i).getValue()));
        }*/
        BarDataSet dataSet = new BarDataSet(entries, "Watt");
        dataSet.setColor(getResources().getColor(R.color.colorAccent));
        dataSet.setBarBorderColor(getResources().getColor(R.color.colorPrimary));

        BarData lineData = new BarData(dataSet);
        dataLineChart.setData(lineData);
        dataLineChart.invalidate();

    }

    private void selectWeekly() {
        weeklyButton.setBackgroundResource(R.color.colorPrimaryDark);

        Random random = new Random();

        List<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            entries.add(new BarEntry(i, 100 + random.nextInt(20)));
        }

        /*for (int i = 0; i < weeklyData.size(); i++) {
            entries.add(new BarEntry(i, weeklyData.get(i).getValue()));
        }*/
        BarDataSet dataSet = new BarDataSet(entries, "Watt");
        dataSet.setColor(getResources().getColor(R.color.colorAccent));
        dataSet.setBarBorderColor(getResources().getColor(R.color.colorPrimary));

        BarData lineData = new BarData(dataSet);
        dataLineChart.setData(lineData);
        dataLineChart.invalidate();

    }

    private void selectDaily() {
        dailyButton.setBackgroundResource(R.color.colorPrimaryDark);

        Random random = new Random();

        List<BarEntry> entries = new ArrayList<>();
        for (int i = 0; i < 7; i++) {
            entries.add(new BarEntry(i, 20 + random.nextInt(8)));
        }

        /*for (int i = 0; i < dailyData.size(); i++) {
            entries.add(new BarEntry(i, dailyData.get(i).getValue()));
        }*/

        BarDataSet dataSet = new BarDataSet(entries, "Watt");
        dataSet.setColor(getResources().getColor(R.color.colorAccent));
        dataSet.setBarBorderColor(getResources().getColor(R.color.colorPrimary));

        BarData lineData = new BarData(dataSet);
        dataLineChart.setData(lineData);
        dataLineChart.invalidate();

    }
}
