package com.ndidevelopment.passivehouse.ui.techsupport;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ndidevelopment.passivehouse.R;
import com.ndidevelopment.passivehouse.models.TechSpecialist;
import com.ndidevelopment.passivehouse.models.User;
import com.ndidevelopment.passivehouse.rest.RetrofitInstance;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Bia on 1/6/2018.
 */

public class TechSupportFragment extends Fragment {

    @BindView(R.id.techsupport_recyclerview_specialists)
    RecyclerView specialistsRecyclerView;

    private List<TechSpecialist> techSpecialistList;
    private TechSupportAdapter techSupportAdapter;
    private RxPermissions rxPermissions;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_techsupport, container, false);
        ButterKnife.bind(this, view);
        initializeRecyler();
        rxPermissions = new RxPermissions(getActivity());
        return view;
    }

    private void initializeRecyler() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        techSpecialistList = new ArrayList<>();
        techSpecialistList.add(new TechSpecialist("Bia Tosa", "An expert in solar energy and the properties of the solar panels.", "0746256478"));
        techSpecialistList.add(new TechSpecialist("Andi Nicolescu", "An expert in solar energy and the properties of the sun.", "0724501383"));


        /*RetrofitInstance.getInstance().getApi()
                .getUsersInfo("Specialist", PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("token", ""))
                .enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        if (response.isSuccessful()) {
                            techSpecialistList.clear();
                            techSpecialistList.addAll(response.body().stream()
                                    .map(u -> new TechSpecialist(u.getFirstName() + " " + u.getLastName(), "", u.getPhoneNumber()))
                                    .collect(Collectors.toList()));
                            techSupportAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {

                    }
                });*/
        techSupportAdapter = new TechSupportAdapter(techSpecialistList, this::callClicked);
        specialistsRecyclerView.setHasFixedSize(true);
        specialistsRecyclerView.setAdapter(techSupportAdapter);
        specialistsRecyclerView.setLayoutManager(linearLayoutManager);
    }

    public void callClicked(int position) {
        rxPermissions.request(Manifest.permission.CALL_PHONE).
                subscribe(granted -> {
                    if (granted) {
                        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + techSpecialistList.get(position).getPhoneNr()));
                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "Need permission to make phone call", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
