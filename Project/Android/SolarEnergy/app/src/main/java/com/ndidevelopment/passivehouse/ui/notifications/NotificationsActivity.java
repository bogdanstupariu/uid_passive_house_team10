package com.ndidevelopment.passivehouse.ui.notifications;

import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ndidevelopment.passivehouse.R;
import com.ndidevelopment.passivehouse.models.Notification;
import com.ndidevelopment.passivehouse.rest.RetrofitInstance;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends AppCompatActivity {

    @BindView(R.id.notifications_recycler)
    RecyclerView notificationRecycler;

    List<Notification> notifications;
    NotificationsAdapter notificationsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);

        notifications = new ArrayList<>();
        notifications.add(new Notification(0, "Please clean the panels today", "0"));
        notifications.add(new Notification(1, "Warranty will expire in 30 days", "1"));
        notifications.add(new Notification(2, "One panel is one fire", "0"));
        notificationsAdapter = new NotificationsAdapter(notifications);

        /*RetrofitInstance.getInstance().getApi().getAllNotifications(PreferenceManager.getDefaultSharedPreferences(this).getString("token", ""))
                .enqueue(new Callback<List<Notification>>() {
                    @Override
                    public void onResponse(Call<List<Notification>> call, Response<List<Notification>> response) {
                        if (response.isSuccessful()) {
                            notifications.clear();
                            notifications.addAll(response.body());
                            notificationsAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Notification>> call, Throwable t) {

                    }
                });*/

        notificationRecycler.setLayoutManager(new LinearLayoutManager(this));
        notificationRecycler.setAdapter(notificationsAdapter);

    }
}
